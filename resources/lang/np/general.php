<?php
return [

	'heading' => [
		  'office'			    => 'जिल्ला पशु सेवा कार्यालय',
			'home'				=> 'गृह पृष्ठ',
			'introduction'		=> 'परिचय',
			'news'				=> 'समाचार',
			'notice'			=> 'सूचना',
			'staff'			    => 'कर्मचारी',
			'fullname'			=> 'पुरा नाम',
			'designation'		=> 'पद',
			'division'			=> 'विभाग',
			'email'			    => 'इमेल',
			'gallery'			=> 'फोटो ग्यालरी',
			'feedback'          => 'सुझाव',
			'feedback-title'    => "सुझाव",
			'contact'           => 'सम्पर्क ठेगाना',
			'contact-title'     => 'सम्पर्क ठेगाना',
			'notices'			=> 'सूचनाहरु',
			'high-lights' 		=> 'हाई लाईटस',
			'useful-links'		=> 'उपयोगी लिङ्कहरू',
			'latest-resources' 	=> 'पछिल्ला सामाग्रीहरु',
			'photo-gallery' 	=> 'फोटो ग्यालरी',
			'published-date'	=> 'प्रकाशित मिति',
			'published-by'		=> 'प्रकासक',
			'phone'				=> 'फोन नं.',
			'fax'				=> 'फ्याक्स नं.',
			'post'				=> 'पो ब. नं.',
			'read-more'			=> 'पुरा पढ्नुहोस',
			'more'				=> 'थप',
		    'no-data-found'     => 'कुनै पनि रेकर्ड भेट्न सकिएन |',


	],
];