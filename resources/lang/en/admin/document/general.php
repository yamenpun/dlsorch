<?php
return [
    'column' => [
        'title_np'              => 'शिर्षक',
        'title_en'              => 'Title',
        'description_np'        => 'विवरण',
        'description_en'        => 'Description',
        'url'                   => 'URL',
        'document'              => 'Document',
        'existing-document'     => 'Existing Document',
        'caption_en'            => 'Caption',
        'caption_np'            => 'क्याप्सन',
        'order'                 => 'Order',
        'status'                => 'Status',
        'user_id'               => 'Created By',
        'action'                => 'Action',
    ],

    'content' => [
        'page'                  => 'Document',
        'page-manager'          => 'Document Manager',
        'list'                  => 'List Document',
        'add'                   => 'Add Form',
        'update'                => 'Update Form',
        'no-document-uploaded'  => 'No document uploaded',
        'active'                => 'Active',
        'inactive'              => 'Inactive',
    ],


    'button' => [
        'add'           => 'Add New',
        'add-submit'    => 'Save',
        'update-submit' => 'Update',
        'reset'         => 'Reset'
    ],
];