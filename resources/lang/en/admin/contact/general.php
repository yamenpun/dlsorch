
<?php
return [

    'column' => [
        'address_np'                  => 'पुरा ठेगाना',
        'address_en'                  => 'Full Address',
        'contact_person_name_np'      => 'सम्पर्क व्यक्तिको नाम',
        'contact_person_name_en'      => 'Contact Person Name',
        'contact_person_post_np'      => 'सम्पर्क व्यक्तिको पद',
        'contact_person_post_en'      => 'Contact Person Post',
        'phone_no'                    => 'Phone Number',
        'fax_no'                      => 'Fax Number',
        'post_box_no'                 => 'Post Box No.',
        'email'                       => 'Email',
        'action'                      => 'Action',
    ],

    'content' => [
        'page'               => 'Contact Page',
        'page-manager'       => 'Contact Page Manager',
        'list'               => 'List Contact Page',
        'add'                => 'Add Form',
        'update'             => 'Update Form',
        'active'             => 'Active',
        'inactive'           => 'Inactive'
    ],


    'button' => [
        'add'                => 'Add New',
        'add-submit'         => 'Save',
        'update-submit'      => 'Update',
        'reset'              => 'Reset'
    ],

];