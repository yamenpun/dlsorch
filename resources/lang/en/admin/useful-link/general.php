<?php
return [
    'column' => [
        'title_np'          => 'शिर्षक',
        'title_en'          => 'Title',
        'url'               => 'URL',
        'order'             => 'Order',
        'created_at'        => 'Created At',
        'updated_at'        => 'Updated At',
        'status'            => 'Status',
        'user_id'           => 'Created By',
        'action'            => 'Action',
    ],

    'content' => [
        'page'              => 'Link',
        'page-manager'      => 'Link Manager',
        'list'              => 'List Link',
        'add'               => 'Add Form',
        'update'            => 'Update Form',
        'no-image-uploaded' => 'No image uploaded',
        'active'            => 'Active',
        'inactive'          => 'Inactive',
    ],


    'button' => [
        'add'           => 'Add New',
        'add-submit'    => 'Save',
        'update-submit' => 'Update',
        'reset'         => 'Reset'
    ],
];