<?php
return [
    'column' => [
        'gallery'               => 'Gallery',
        'image'                 => 'Image',
        'caption_np'            => 'क्याप्सन ',
        'caption_en'            => 'Caption',
        'url'                   => 'URL',
        'order'                 => 'Order',
        'status'                => 'Status',
        'action'                => 'Action',
    ],

    'content' => [
        'page'                  => 'Gallery Images',
        'page-manager'          => 'Gallery Images Manager',
        'list'                  => 'List Gallery Images',
        'add'                   => 'Add Form',
        'update'                => 'Update Form',
        'active'                => 'Active',
        'inactive'              => 'Inactive',
    ],


    'button' => [
        'add'           => 'Add New',
        'add-submit'    => 'Save',
        'update-submit' => 'Update',
        'reset'         => 'Reset'
    ],
];