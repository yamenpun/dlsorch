<?php
return [
    'column' => [
        'title_np'              => 'शिर्षक',
        'title_en'              => 'Title',
        'description_np'        => 'विवरण',
        'description_en'        => 'Description',
        'image'                 => 'Image',
        'existing-image'        =>'Existing Image',
        'url'                   => 'URL',
        'order'                 => 'Order',
        'status'                => 'Status',
        'action'                => 'Action',
    ],

    'content' => [
        'page'                  => 'Gallery',
        'page-manager'          => 'Gallery Manager',
        'list'                  => 'List Gallery',
        'no-description'        => 'No Image Uploaded',
        'no-image-uploaded'     => 'No Image Uploaded',
        'add'                   => 'Add Form',
        'update'                => 'Update Form',
        'active'                => 'Active',
        'inactive'              => 'Inactive',
    ],


    'button' => [
        'add'           => 'Add New',
        'add-submit'    => 'Save',
        'update-submit' => 'Update',
        'reset'         => 'Reset'
    ],
];