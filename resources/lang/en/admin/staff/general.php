<?php
return [
    'column' => [
        'designation_np'    => 'पद',
        'designation_en'    => 'Designation',
        'fullname_np'       => 'पुरा नाम',
        'fullname_en'       => 'Fullname',
        'division_np'       => 'विभाग',
        'division_en'       => 'Division',
        'email'             => 'Email',
        'phone_no'          => 'Mobile',
        'image'             => 'Image',
        'order'             => 'Order',
        'created_at'        => 'Created At',
        'updated_at'        => 'Updated At',
        'status'            => 'Status',
        'user_id'           => 'Created By',
        'action'            => 'Action',
    ],

    'content' => [
        'page'              => 'Staff',
        'page-manager'      => 'Staff Manager',
        'list'              => 'List Staff',
        'add'               => 'Add Form',
        'update'            => 'Update Form',
        'no-image-uploaded' => 'No image uploaded',
        'active'            => 'Active',
        'inactive'          => 'Inactive',
    ],


    'button' => [
        'add'           => 'Add New',
        'add-submit'    => 'Save',
        'update-submit' => 'Update',
        'reset'         => 'Reset'
    ],
];