
<?php
return [

    'column' => [
        'title_np'                  => 'शिर्षक',
        'title_en'                  => 'Title',
        'description_np'            => 'विवरण',
        'description_en'            => 'Description',
        'created_at'                => 'Created At',
        'updated_at'                => 'Updated At',
        'action'                    => 'Action',
    ],

    'content' => [
        'page'               => 'Introduction Page',
        'page-manager'       => 'Introduction Page Manager',
        'list'               => 'List Introduction Page',
        'add'                => 'Add Form',
        'update'             => 'Update Form',
        'active'             => 'Active',
        'inactive'           => 'Inactive'
    ],


    'button' => [
        'add'                => 'Add New',
        'add-submit'         => 'Save',
        'update-submit'      => 'Update',
        'reset'              => 'Reset'
    ],

];