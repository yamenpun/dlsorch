<?php
return [
    'column' => [
        'title_np'          => 'शिर्षक',
        'title_en'          => 'Title',
        'url'               => 'URL',
        'order'             => 'Order',
        'status'            => 'Status',
        'action'            => 'Action',
    ],

    'content' => [
        'page'          => 'Link Page',
        'page-manager'  => 'Link Page Manager',
        'list'          => 'List Link Page',
        'add'           => 'Add Form',
        'update'        => 'Update Form',
        'active'        => 'Active',
        'inactive'      => 'Inactive',
    ],


    'button' => [
        'add'           => 'Add New',
        'add-submit'    => 'Save',
        'update-submit' => 'Update',
        'reset'         => 'Reset'
    ],

    'message' => [
        'add' => 'The record added successfully.'
    ],
];