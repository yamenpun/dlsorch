<?php
return [
    'column' => [
        'title'         => 'Title',
        'created_at'    => 'Created at',
        'updated_at'    => 'Update at',
        'status'        => 'Status',
        'action'        => 'Action',
        'active'        => 'Active',
        'deactive'      => 'Deactive',
    ],

    'content' => [
        'page'          => 'Tag',
        'page-manager'  => 'Tag Manager',
        'list'          => 'Tag List',
        'add'           => 'Tag Form',
        'update'        => 'Update Form'
    ],


    'button' => [
        'add'           => 'Add',
        'add_submit'    => 'Save',
        'update_submit' => 'Update',
        'reset'         => 'Reset'
    ],
];