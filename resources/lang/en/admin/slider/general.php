<?php
return [
    'column' => [
        'caption_np'       => 'क्याप्सन',
        'caption_en'       => 'Caption',
        'image'             => 'Image',
        'existing-image'    => 'Existing Image',
        'url'               => 'URL',
        'order'             => 'Order',
        'description_np'    => 'विवरण',
        'description_en'    => 'Description',
        'created_at'        => 'Created At',
        'updated_at'        => 'Updated At',
        'status'            => 'Status',
        'action'            => 'Action',
    ],

    'content' => [
        'page'              => 'Slider',
        'page-manager'      => 'Slider Manager',
        'list'              => 'List Slider',
        'add'               => 'Add Form',
        'update'            => 'Update Form',
        'no-image-uploaded' => 'No image uploaded',
        'active'            => 'Active',
        'inactive'          => 'Inactive',
    ],


    'button' => [
        'add'           => 'Add New',
        'add-submit'    => 'Save',
        'update-submit' => 'Update',
        'reset'         => 'Reset'
    ],

    'message' => [
        'add' => 'The Slider added successfully.'
    ],
];