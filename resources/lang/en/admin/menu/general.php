<?php
return [
    'column' => [
        'title'             => 'Title',
        'position'          => 'Position',
        'status'            => 'Status',
        'order'             => 'Order',
        'action'            => 'Action',
        'page'              => 'Pages',
        'parent-page'       => 'Parent Page',
        'select-page'       => 'Select Page',
    ],

    'content' => [
        'page'              => 'Menu',
        'page-manager'      => 'Menu Manager',
        'list'              => 'List Menu',
        'add'               => 'Add Form',
        'update'            => 'Update Form',
        'active'            => 'Active',
        'inactive'          => 'Inactive',

    ],


    'button' => [
        'add'           => 'Add New',
        'add-submit'    => 'Save',
        'update-submit' => 'Update',
        'reset'         => 'Reset'
    ],
];