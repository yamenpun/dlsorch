<?php
return [
    'column' => [
        'fullname_np'               => 'पुरा नाम',
        'fullname_en'               => 'Full Name',
        'username'                  => 'Username',
        'email'                     => 'Email',
        'password'                  => 'Password',
        'password-confirmation'     => 'Confirm Password',
        'new-password'              => 'New Password',
        'created-at'                => 'Created At',
        'updated-at'                => 'Updated At',
        'role'                      => 'User Type',
        'status'                    => 'Status',
        'action'                    => 'Action',
    ],

    'content' => [
        'page'                      => 'User',
        'page-manager'              => 'User Manager',
        'profile'                   => 'Profile',
        'user-profile'              => 'User Profile',
        'list'                      => 'List User',
        'detail'                    => 'User Details',
        'add'                       => 'Add Form',
        'update'                      => 'Update Form',
        'password-confirmation'     => 'Retype Password',
        'active'                    => 'Active',
        'inactive'                  => 'Inactive',
    ],


    'button' => [
        'add'           => 'Add New',
        'add-submit'    => 'Save',
        'update-submit' => 'Update',
        'reset'         => 'Reset'
    ],
];