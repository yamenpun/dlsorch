<?php
return [
    'column' => [
        'title_np'          => 'शिर्षक',
        'title_en'          => 'Title',
        'description_np'    => 'विवरण',
        'description_en'    => 'Description',
        'url'               => 'URL',
        'image'             => 'Image',
        'caption_en'        => 'Caption',
        'caption_np'        => 'क्याप्सन',
        'image_url'         => 'Image URL',
        'order'             => 'Order',
        'status'            => 'Status',
        'user_id'           => 'Created By',
        'action'            => 'Action',
    ],

    'content' => [
        'page'              => 'Notice',
        'page-manager'      => 'Notice Manager',
        'list'              => 'List Notice',
        'add'               => 'Add Form',
        'update'            => 'Update Form',
        'no-image-uploaded' => 'No image uploaded',
        'active'            => 'Active',
        'inactive'          => 'Inactive',
    ],


    'button' => [
        'add'           => 'Add New',
        'add-submit'    => 'Save',
        'update-submit' => 'Update',
        'reset'         => 'Reset'
    ],
];