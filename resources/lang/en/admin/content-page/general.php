
<?php
return [

    'column' => [
        'title_np'          => 'शिर्षक',
        'title_en'          => 'Title',
        'content_np'        => 'कन्टेन्ट',
        'content_en'        => 'Content',
        'url'               => 'URL',
        'image'             => 'Image',
        'page_type'         => 'Page Type',
        'order'             => 'Order',
        'status'            => 'Status',
        'action'            => 'Action',
    ],

    'content' => [
        'page'               => 'Content Page',
        'page-manager'       => 'Content Page Manager',
        'list'               => 'List Content Page',
        'add'                => 'Add Form',
        'update'             => 'Update Form',
        'active'             => 'Active',
        'inactive'           => 'Inactive'
    ],


    'button' => [
        'add'                => 'Add New',
        'add-submit'         => 'Save',
        'update-submit'      => 'Update',
        'reset'              => 'Reset'
    ],

];