<?php
return [

	'home' => 'HOME',

	'common' => [
		'no-content-found' 	       => 'No Content Found.',
		'no-description-found' 	   => 'No Description Found.',
		'no-data-found' 	       => 'No Data Found.',
		'no-image-found' 	       => 'No Image Uploaded.',
		'no-caption-found' 	       => 'No Caption Found.',
		'no-document-found'        => 'No Document Uploaded.'

	],

    'heading' => [
		'office'			   => 'District Office Of Livestock Services',
    	'home' 			       => 'Home',
		'introduction'		   => 'Introduction',
		'news'                 => 'News',
		'notice'               => 'Notice',
		'staff'                => 'Staff',
		'fullname'			   => 'Fullname',
		'designation'		   => 'Designation',
		'division'			   => 'Division',
		'email'			       => 'Email',
		'gallery'			   => 'Gallery',
		'feedback'             => "Feedback",
		'feedback-title'       => "Send Us Your Feedback",
		'contact'              => 'Contact Us',
		'contact-title'        => 'Contact Information',
    	'notices' 			   => 'Notices',
	    'high-lights' 		   => 'HIGHLIGHTS',
		'useful-links'	       => 'Useful Links',
		'latest-resources' 	   => 'Latest Resources',
		'photo-gallery' 	   => 'Photo Gallery',
		'published-date'	   => 'Published Date',
		'published-by'		   => 'Published by',
		'phone'				   => 'Phone',
		'fax'				   => 'Fax',
		'post'				   => 'Post Box',
		'read-more'			   => 'Read More',
		'more'				   => 'More',
		'no-data-found'        => 'No Records are found.'
	],
];