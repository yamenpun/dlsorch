<div class="sidebar" id="sidebar">

    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    </script>

    <ul class="nav nav-list">

        <li {!! Request::is('admin/dashboard')?'class="active"':'' !!}>
            <a href="{{ route('admin.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
        </li>

        <li {!! Request::is('admin/menu*')?'class="active"':'' !!}>
            <a href="{{ route('admin.menu') }}">
                <i class="icon-list"></i>
                <span class="menu-text"> Menu </span>
            </a>
        </li>

        <li {!! Request::is('admin/content-page*') || Request::is('admin/link-page*')?'class="active open"':'' !!}>
            <a class="dropdown-toggle" href="#">
                <i class="icon-file-text"></i>
                <span class="menu-text"> Page </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">
                <li {!! Request::is('admin/content-page*')?'class="active"':'' !!}>
                    <a href="{{ route('admin.content-page') }}">
                        <i class="icon-double-angle-right"></i>
                        Content Page
                    </a>
                </li>

                <li {!! Request::is('admin/link-page*')?'class="active"':'' !!}>
                    <a href="{{ route('admin.link-page') }}">
                        <i class="icon-double-angle-right"></i>
                        Link Page
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('admin/introduction*')?'class="active"':'' !!}>
            <a href="{{ route('admin.introduction') }}">
                <i class="icon-bullhorn"></i>
                <span class="menu-text"> Introduction </span>
            </a>
        </li>

        <li {!! Request::is('admin/slider*')?'class="active"':'' !!}>
            <a href="{{ route('admin.slider') }}">
                <i class="icon-exchange"></i>
                <span class="menu-text"> Slider </span>
            </a>
        </li>

        <li {!! Request::is('admin/news*')?'class="active"':'' !!}>
            <a href="{{ route('admin.news') }}">
                <i class=" icon-globe"></i>
                <span class="menu-text"> News </span>
            </a>
        </li>

        <li {!! Request::is('admin/notice*')?'class="active"':'' !!}>
            <a href="{{ route('admin.notice') }}">
                <i class="icon-info-sign"></i>
                <span class="menu-text"> Notice </span>
            </a>
        </li>

        <li {!! Request::is('admin/staff*')?'class="active"':'' !!}>
            <a href="{{ route('admin.staff') }}">
                <i class="icon-group"></i>
                <span class="menu-text"> Staff </span>
            </a>
        </li>

        <li {!! Request::is('admin/gallery*')?'class="active"':'' !!}>
            <a href="{{ route('admin.gallery') }}">
                <i class="icon-list"></i>
                <span class="menu-text"> Gallery </span>
            </a>
        </li>

        <li {!! Request::is('admin/images*')?'class="active"':'' !!}>
            <a href="{{ route('admin.images') }}">
                <i class="icon-camera"></i>
                <span class="menu-text"> Gallery Images </span>
            </a>
        </li>

        <li {!! Request::is('admin/useful-link*')?'class="active"':'' !!}>
            <a href="{{ route('admin.useful-link') }}">
                <i class=" icon-external-link"></i>
                <span class="menu-text"> Useful Link </span>
            </a>
        </li>

        <li {!! Request::is('admin/document*')?'class="active"':'' !!}>
            <a href="{{ route('admin.document') }}">
                <i class="icon-folder-close"></i>
                <span class="menu-text"> Document </span>
            </a>
        </li>

        <li {!! Request::is('admin/contact*')?'class="active"':'' !!}>
            <a href="{{ route('admin.contact') }}">
                <i class="icon-envelope"></i>
                <span class="menu-text"> Contact </span>
            </a>
        </li>

        <li {!! Request::is('admin/user*')?'class="active"':'' !!}>
            <a href="{{ route('admin.user') }}">
                <i class="icon-user"></i>
                <span class="menu-text"> User </span>
            </a>
        </li>

        <li>
            <a href="{{ url('logout') }}">
                <i class="icon-signout"></i>
                <span class="menu-text"> LogOut </span>
            </a>
        </li>

    </ul>

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>

</div>
