<div class="navbar navbar-default" id="navbar">

    <script type="text/javascript">
        try {
            ace.settings.check('navbar', 'fixed')
        } catch (e) {
        }
    </script>

    <div class="navbar-container" id="navbar-container">

        <div class="navbar-header pull-left">

            <a href="{{ route('admin.dashboard') }}" class="navbar-brand">
                <small><img src="{{ asset('assets/frontend/images/logo.png') }}">
                    DLSO Ramechhap
                </small>
            </a>

        </div>

        <div class="navbar-header pull-right" role="navigation">

            <ul class="nav ace-nav">

                <li class="light-blue">

                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">

                        <img class="nav-user-photo" src="{{ asset('assets/admin/avatars/user.png') }}" alt="Jason's Photo"/>
								<span class="user-info">
									<small>Welcome,</small>
                                    @if(isset($logged_in_user->username))
                                        {{ $logged_in_user->username }}
                                    @endif
								</span>

                        <i class="icon-caret-down"></i>
                    </a>

                    <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

                        <li>
                            <a href="{{ route('admin.user.profile') }}">
                                <i class="icon-user"></i>
                                Profile
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="{{ url('logout') }}">
                                <i class="icon-off"></i>
                                Logout
                            </a>
                        </li>

                    </ul>

                </li>

            </ul>

        </div>

    </div>

</div>