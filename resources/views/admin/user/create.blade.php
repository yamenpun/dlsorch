@extends('admin.layouts.master')

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
                </li>

                <li>
                    <a href="{{ route($base_route) }}">{{ trans($trans_path.'general.content.page') }}</a>
                </li>

                <li class="active">{{ trans($trans_path.'general.content.add') }}</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    {{ trans($trans_path.'general.content.page-manager') }}

                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'general.content.add') }}
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="">

                    <!-- PAGE CONTENT BEGINS -->

                    {!! Form::open([
                    'route' => $base_route.'.store',
                    'method' => 'post',
                    'class' => 'form-horizontal',
                    'role' => "form",
                    'enctype' => "multipart/form-data"
                    ]) !!}

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="">

                        <div class="tabbable tabs-left">

                            <ul class="nav nav-tabs" id="myTab3">

                                <li class="active">
                                    <a data-toggle="tab" href="#data">
                                        Data
                                    </a>
                                </li>

                                <li class="">
                                    <a data-toggle="tab" href="#general">
                                        General
                                    </a>
                                </li>

                            </ul>

                            <div class="tab-content">

                                <div id="data" class="tab-pane active">

                                    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">

                                        <li class="active">
                                            <a data-toggle="tab" href="#nepali_tab"><img src="{{ asset('assets/admin/ne.gif') }}"> नेपाली</a>
                                        </li>

                                        <li class="">
                                            <a data-toggle="tab" href="#english_tab"><img src="{{ asset('assets/admin/en.jpg') }}"> English</a>
                                        </li>

                                    </ul>

                                    <div class="tab-content">

                                        <!-- Nepali Tab Start -->

                                        <div id="nepali_tab" class="tab-pane active">

                                            <div class="form-group">

                                                <label class="col-sm-2 control-label no-padding-right" for="fullname_np"> {{ trans($trans_path.'general.column.fullname_np') }} </label>

                                                <div class="col-sm-3">

                                                    {!! Form::text('fullname_np', null, [
                                                       "'kl_virtual_keyboard_secure_input" => "on",
                                                       'id' => 'fullname_np',
                                                       "placeholder" => "पुरा नाम",
                                                       "class" => "col-xs-10 col-sm-12",
                                                    ]) !!}

                                                    {!! AppHelper::getValidationErrorMsg($errors, 'fullname_np') !!}

                                                </div>

                                            </div>

                                        </div>

                                        <!-- English Tab Start -->

                                        <div id="english_tab" class="tab-pane">

                                            <div class="form-group">

                                                <label class="col-sm-2 control-label no-padding-right" for="fullname_en"> {{ trans($trans_path.'general.column.fullname_en') }} </label>

                                                <div class="col-sm-3">

                                                    {!! Form::text('fullname_en', null, [
                                                       "'kl_virtual_keyboard_secure_input" => "on",
                                                       'id' => 'fullname_en',
                                                       "placeholder" => "Fullname",
                                                       "class" => "col-xs-10 col-sm-12",
                                                    ]) !!}

                                                    {!! AppHelper::getValidationErrorMsg($errors, 'fullname_en') !!}

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <!-- General Tab Content -->

                                <div id="general" class="tab-pane">

                                    <div class="space-4"></div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right" for="username"> {{ trans($trans_path.'general.column.username') }} </label>

                                        <div class="col-sm-3">

                                            {!! Form::text('username', null, [
                                               "'kl_virtual_keyboard_secure_input" => "on",
                                               'id' => 'username',
                                               "placeholder" => "Username",
                                               "class" => "col-xs-10 col-sm-12",
                                            ]) !!}

                                            {!! AppHelper::getValidationErrorMsg($errors, 'username') !!}

                                        </div>

                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right" for="email"> {{ trans($trans_path.'general.column.email') }} </label>

                                        <div class="col-sm-3">

                                            {!! Form::email('email', null, [
                                               "'kl_virtual_keyboard_secure_input" => "on",
                                               'id' => 'email',
                                               "placeholder" => "Email",
                                               "class" => "col-xs-10 col-sm-12",
                                            ]) !!}

                                            {!! AppHelper::getValidationErrorMsg($errors, 'email') !!}

                                        </div>

                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right" for="password"> {{ trans($trans_path.'general.column.password') }} </label>

                                        <div class="col-sm-3">

                                            {!! Form::password('password', null, [
                                              "kl_virtual_keyboard_secure_input"   => "on",
                                              'id'                                 => 'password',
                                              "placeholder"                        => "Password",
                                              "class"                              => "col-xs-10 col-sm-12",
                                           ]) !!}

                                            {!! AppHelper::getValidationErrorMsg($errors, 'password') !!}

                                        </div>

                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right" for="password_confirmation"> {{ trans($trans_path.'general.column.password-confirmation') }} </label>

                                        <div class="col-sm-3">

                                            {!! Form::password('password_confirmation', null, [
                                              "kl_virtual_keyboard_secure_input"   => "on",
                                              'id'                                 => 'password_confirmation',
                                              "class"                              => "col-xs-10 col-sm-12",
                                           ]) !!}

                                            {!! AppHelper::getValidationErrorMsg($errors, 'password_confirmation') !!}

                                        </div>

                                    </div>


                                    <div class="space-4"></div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right" for="role"> {{ trans($trans_path.'general.column.role') }} </label>

                                        <div class="col-sm-3">

                                            <select name="role" id="role" class="col-xs-10 col-sm-12">

                                                <option value="administrator">Administrator</option>
                                                <option value="normal" selected="selected">Normal</option>

                                            </select>

                                        </div>

                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right" for="status"> {{ trans($trans_path.'general.column.status') }} </label>

                                        <div class="col-sm-3">

                                            <div class="control-group">

                                                <div class="radio">

                                                    <label>
                                                        {!! Form::radio('status', 1, true, [
                                                            'class' => 'ace'
                                                        ]) !!}
                                                        <span class="lbl">Active</span>
                                                    </label>

                                                </div>

                                                <div class="radio">

                                                    <label>
                                                        {!! Form::radio('status', 0, false, [
                                                             'class' => 'ace'
                                                         ]) !!}
                                                        <span class="lbl"> Inactive </span>
                                                    </label>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="clearfix form-actions">

                        <div class="col-md-offset-3 col-md-9">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                {{ trans($trans_path.'general.button.add-submit') }}
                            </button>

                            &nbsp; &nbsp; &nbsp;

                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                {{ trans($trans_path.'general.button.reset') }}
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

@endsection