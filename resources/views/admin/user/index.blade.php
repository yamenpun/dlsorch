@extends('admin.layouts.master')

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
                </li>

                <li class="active">{{ trans($trans_path.'general.content.page') }}</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    {{ trans($trans_path.'general.content.page-manager') }}

                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'general.content.list') }}
                    </small>

                    <a href="{{ route($base_route.'.add') }}" class="btn btn-primary btn-sm">
                        <i class="icon-plus-sign bigger-110"></i>
                        {{ trans($trans_path.'general.button.add') }}
                    </a>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">

                        <div class="col-xs-12">

                            <div class="table-responsive">

                                @if (session()->has('message'))
                                    {!! session()->get('message') !!}
                                @endif

                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">

                                    <thead>

                                        <tr>

                                            <th style="width: 20px;" class="center">
                                                <label>
                                                    <input class="ace" type="checkbox">
                                                    <span class="lbl"></span>
                                                </label>
                                            </th>

                                            <th style="width: 150px;">{{ trans($trans_path.'general.column.fullname_en') }}</th>

                                            <th style="width: 100px;">{{ trans($trans_path.'general.column.username') }}</th>

                                            <th style="width: 100px;">{{ trans($trans_path.'general.column.email') }}</th>

                                            <th style="width: 100px;">{{ trans($trans_path.'general.column.role') }}</th>

                                            <th style="width: 20px;">{{ trans($trans_path.'general.column.status') }}</th>

                                            <th style="width: 20px;">{{ trans($trans_path.'general.column.action') }}</th>

                                        </tr>

                                    </thead>

                                    <tbody>

                                    @if ($data['rows']->count() > 0)
                                        @foreach($data['rows'] as $row)

                                            <tr>

                                                <td class="center">
                                                    <label>
                                                        <input class="ace" type="checkbox">
                                                        <span class="lbl"></span>
                                                    </label>
                                                </td>

                                                <td>{{ $row->fullname_np }} <br/> {{ $row->fullname_en }} </td>

                                                <td>{{ $row->username }} </td>

                                                <td>{{ $row->email }} </td>

                                                <td>{{ $row->role }} </td>

                                                <td>
                                                    @if($row->status == 1)
                                                        <button class="btn btn-minier btn-primary">{{ trans($trans_path.'general.content.active') }}</button>
                                                    @else
                                                        <button class="btn btn-minier btn-yellow">{{ trans($trans_path.'general.content.inactive') }}</button>
                                                    @endif
                                                </td>

                                                <td>

                                                    <div class="btn-group">

                                                        <a href="{{ route($base_route.'.edit', ['id' => $row->id]) }}">
                                                            <button class="btn btn-xs btn-info">
                                                                <i class="icon-edit bigger-120"></i>
                                                            </button>
                                                        </a>

                                                        <a  href="{{ route($base_route.'.delete', ['id' => $row->id]) }}">
                                                            <button class="btn btn-xs btn-danger deleteConfirm">
                                                                <i class="icon-trash bigger-120"></i>
                                                            </button>
                                                        </a>

                                                    </div>

                                                </td>

                                            </tr>

                                        @endforeach

                                        <tr>
                                            <td colspan="8">{{ $data['rows']->links() }}</td>
                                        </tr>

                                    @else

                                        <tr>
                                            <td colspan="8">{{ trans('general.common.no-data-found') }}</td>
                                        </tr>

                                    @endif

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                    <div class="hr hr-18 dotted hr-double"></div>

                </div>

            </div>

        </div>

    </div>

@endsection
