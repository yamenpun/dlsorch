@extends('admin.layouts.master')

@section('content')

   <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="#">{{ trans('general.home') }}</a>
                </li>

                <li class="active"> {{ trans($trans_path.'general.content.profile') }} </li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>
                    {{ trans($trans_path.'general.content.user-profile') }}
                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <!-- PAGE CONTENT BEGINS -->

                    <div>
                        <div id="user-profile-1" class="user-profile row">

                            <div class="col-xs-12 col-sm-3 center">

                                <div>

                                    <span class="profile-picture">
                                        <img id="avatar" class="editable img-responsive editable-click editable-empty" alt="Alex's Avatar" 
                                        src="{{ asset(config('broadway.url.main').'assets/admin/avatars/profile.png') }}"></img>
                                    </span>

                                    <div class="space-4"></div>

                                    <div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">

                                        <div class="inline position-relative">

                                            <a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-circle light-green middle"></i>
                                                &nbsp;
                                                <span class="white">{{ $data['rows']->fullname_en }}</span>
                                            </a>

                                        </div>

                                    </div>

                                </div>

                                <div class="space-6"></div>

                                <div class="profile-contact-info"></div>

                                <div class="hr hr16 dotted"></div>

                            </div>

                            <div class="col-xs-12 col-sm-9">
                                
                                <div class="profile-user-info profile-user-info-striped">
                                    
                                    <div class="profile-info-row">

                                        <div class="profile-info-name"> {{ trans($trans_path.'general.column.fullname_en') }} </div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="username">{{ $data['rows']->fullname_en }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name"> {{ trans($trans_path.'general.column.username') }} </div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="username">{{ $data['rows']->username }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name"> {{ trans($trans_path.'general.column.email') }} </div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="username">{{ $data['rows']->email }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name"> {{ trans($trans_path.'general.column.role') }} </div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="age">{{ $data['rows']->role }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name"> {{ trans($trans_path.'general.column.created-at') }} </div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="signup">{{ date('jS M, Y', strtotime($data['rows']->created_at)) }}</span>
                                        </div>

                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name"> {{ trans($trans_path.'general.column.updated-at') }} </div>

                                        <div class="profile-info-value">
                                            <span class="editable editable-click" id="signup">{{ date('jS M, Y', strtotime($data['rows']->updated_at)) }}</span>
                                        </div>

                                    </div>

                                </div>

                                <div class="space-6"></div>

                            </div>

                        </div>

                    </div>

                    <!-- PAGE CONTENT ENDS -->
                </div>

            </div>

        </div>

    </div>

@endsection