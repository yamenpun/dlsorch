@extends('admin.layouts.master')

@section('page_specific_style')

    <style>
        .acb {
            display: block;
        }
    </style>

    @endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
                </li>

                <li class="active">{{ trans($trans_path.'general.content.page') }}</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>
                    {{ trans($trans_path.'general.content.page-manager') }}

                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'general.content.welcome') }}
                    </small>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <!-- PAGE CONTENT BEGINS -->

                    <div class="alert alert-block alert-success">

                        <button type="button" class="close" data-dismiss="alert">
                            <i class="icon-remove"></i>
                        </button>

                        <i class="icon-ok green"></i>

                        {{ trans($trans_path.'general.content.welcome-message') }}

                    </div>

                    <!-- PAGE CONTENT ENDS -->

                </div>

            </div>

        </div>

    </div>

    @endsection

