@extends('admin.layouts.master')

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
                </li>

                <li>
                    <a href="{{ route($base_route) }}">{{ trans($trans_path.'general.content.page') }}</a>
                </li>

                <li class="active">{{ trans($trans_path.'general.content.update') }}</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    {{ trans($trans_path.'general.content.page-manager') }}

                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'general.content.update') }}
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="">
                    <!-- PAGE CONTENT BEGINS -->

                    {!! Form::model($data['row'], [
                    'route' => [$base_route.'.update', $data['row']->id],
                    'method' => 'post',
                    'class' => 'form-horizontal',
                    'role' => "form",
                    'enctype' => "multipart/form-data"
                    ]) !!}

                    <input type="hidden" name="id" value="{{ $data['row']->id }}">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="">

                        <div class="tabbable tabs-left">

                            <ul class="nav nav-tabs" id="myTab3">

                                <li class="active">
                                    <a data-toggle="tab" href="#data">
                                        Data
                                    </a>
                                </li>

                                <li class="">
                                    <a data-toggle="tab" href="#general">
                                        General
                                    </a>
                                </li>

                            </ul>

                            <div class="tab-content">

                                <div id="data" class="tab-pane active">

                                    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">

                                        <li class="active">
                                            <a data-toggle="tab" href="#nepali_tab"><img src="{{ asset('assets/admin/ne.gif') }}"> नेपाली</a>
                                        </li>

                                        <li class="">
                                            <a data-toggle="tab" href="#english_tab"><img src="{{ asset('assets/admin/en.jpg') }}"> English</a>
                                        </li>

                                    </ul>

                                    <div class="tab-content">

                                        <!-- Nepali Tab Start -->
                                        <div id="nepali_tab" class="tab-pane active">

                                            <div class="form-group">

                                                <label class="col-sm-2 control-label no-padding-right" for="address_np"> {{ trans($trans_path.'general.column.address_np') }} </label>

                                                <div class="col-sm-3">

                                                    {!! Form::text('address_np', null, [
                                                       "'kl_virtual_keyboard_secure_input" => "on",
                                                       'id' => 'address_np',
                                                       "placeholder" => "पुरा ठेगाना",
                                                       "class" => "col-xs-10 col-sm-12",
                                                    ]) !!}

                                                    {!! AppHelper::getValidationErrorMsg($errors, 'address_np') !!}

                                                </div>

                                            </div>

                                            <div class="space-4"></div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label no-padding-right" for="contact_person_name_np"> {{ trans($trans_path.'general.column.contact_person_name_np') }} </label>

                                                <div class="col-sm-3">

                                                    {!! Form::text('contact_person_name_np', null, [
                                                       "'kl_virtual_keyboard_secure_input" => "on",
                                                       'id' => 'contact_person_name_np',
                                                       "placeholder" => "सम्पर्क व्यक्तिको नाम",
                                                       "class" => "col-xs-10 col-sm-12",
                                                    ]) !!}

                                                    {!! AppHelper::getValidationErrorMsg($errors, 'contact_person_name_np') !!}

                                                </div>

                                            </div>

                                            <div class="space-4"></div>

                                            <div class="form-group">

                                                <label class="col-sm-2 control-label no-padding-right" for="contact_person_post_np"> {{ trans($trans_path.'general.column.contact_person_post_np') }} </label>

                                                <div class="col-sm-3">

                                                    {!! Form::text('contact_person_post_np', null, [
                                                       "'kl_virtual_keyboard_secure_input" => "on",
                                                       'id' => 'contact_person_post_np',
                                                       "placeholder" => "सम्पर्क व्यक्तिको पद",
                                                       "class" => "col-xs-10 col-sm-12",
                                                    ]) !!}

                                                    {!! AppHelper::getValidationErrorMsg($errors, 'contact_person_post_np') !!}

                                                </div>

                                            </div>

                                        </div>

                                        <!-- English Tab Start -->
                                        <div id="english_tab" class="tab-pane">

                                            <div class="form-group">

                                                <label class="col-sm-2 control-label no-padding-right" for="address_en"> {{ trans($trans_path.'general.column.address_en') }} </label>
                                                <div class="col-sm-3">

                                                    {!! Form::text('address_en', null, [
                                                       "'kl_virtual_keyboard_secure_input" => "on",
                                                       'id' => 'address_en',
                                                       "placeholder" => "Full Address",
                                                       "class" => "col-xs-10 col-sm-12",
                                                    ]) !!}

                                                    {!! AppHelper::getValidationErrorMsg($errors, 'address_en') !!}

                                                </div>

                                            </div>

                                            <div class="space-4"></div>

                                            <div class="form-group">

                                                <label class="col-sm-2 control-label no-padding-right" for="contact_person_name_en"> {{ trans($trans_path.'general.column.contact_person_name_en') }} </label>

                                                <div class="col-sm-3">
                                                    {!! Form::text('contact_person_name_en', null, [
                                                       "'kl_virtual_keyboard_secure_input" => "on",
                                                       'id' => 'contact_person_name_en',
                                                       "placeholder" => "Contact Person Name",
                                                       "class" => "col-xs-10 col-sm-12",
                                                    ]) !!}

                                                    {!! AppHelper::getValidationErrorMsg($errors, 'contact_person_name_en') !!}

                                                </div>

                                            </div>

                                            <div class="space-4"></div>

                                            <div class="form-group">

                                                <label class="col-sm-2 control-label no-padding-right" for="contact_person_post_en"> {{ trans($trans_path.'general.column.contact_person_post_en') }} </label>

                                                <div class="col-sm-3">

                                                    {!! Form::text('contact_person_post_en', null, [
                                                       "'kl_virtual_keyboard_secure_input" => "on",
                                                       'id' => 'contact_person_post_en',
                                                       "placeholder" => "Contact Person Post",
                                                       "class" => "col-xs-10 col-sm-12",
                                                    ]) !!}

                                                    {!! AppHelper::getValidationErrorMsg($errors, 'contact_person_post_en') !!}

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <!-- General Tab Content -->
                                <div id="general" class="tab-pane">

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right" for="phone_no"> {{ trans($trans_path.'general.column.phone_no') }} </label>

                                        <div class="col-sm-3">

                                            {!! Form::text('phone_no', null, [
                                               "'kl_virtual_keyboard_secure_input" => "on",
                                               'id' => 'phone_no',
                                               "placeholder" => "Phone Number",
                                               "class" => "col-xs-10 col-sm-12",
                                            ]) !!}

                                            {!! AppHelper::getValidationErrorMsg($errors, 'phone_no') !!}

                                        </div>

                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right" for="fax_no"> {{ trans($trans_path.'general.column.fax_no') }} </label>

                                        <div class="col-sm-3">

                                            {!! Form::text('fax_no', null, [
                                               "'kl_virtual_keyboard_secure_input" => "on",
                                               'id' => 'fax_no',
                                               "placeholder" => "Fax Number",
                                               "class" => "col-xs-10 col-sm-12",
                                            ]) !!}

                                            {!! AppHelper::getValidationErrorMsg($errors, 'fax_no') !!}

                                        </div>

                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right" for="post_box_no"> {{ trans($trans_path.'general.column.post_box_no') }} </label>

                                        <div class="col-sm-3">

                                            {!! Form::text('post_box_no', null, [
                                               "'kl_virtual_keyboard_secure_input" => "on",
                                               'id' => 'post_box_no',
                                               "placeholder" => "Post Box Number",
                                               "class" => "col-xs-10 col-sm-12",
                                            ]) !!}

                                            {!! AppHelper::getValidationErrorMsg($errors, 'post_box_no') !!}

                                        </div>

                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right" for="email"> {{ trans($trans_path.'general.column.email') }} </label>

                                        <div class="col-sm-3">

                                            {!! Form::text('email', null, [
                                               "'kl_virtual_keyboard_secure_input" => "on",
                                               'id' => 'email',
                                               "placeholder" => "Email",
                                               "class" => "col-xs-10 col-sm-12",
                                            ]) !!}

                                            {!! AppHelper::getValidationErrorMsg($errors, 'email') !!}

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="clearfix form-actions">

                        <div class="col-md-offset-3 col-md-9">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                {{ trans($trans_path.'general.button.update-submit') }}
                            </button>

                            &nbsp; &nbsp; &nbsp;

                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                {{ trans($trans_path.'general.button.reset') }}
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

@endsection