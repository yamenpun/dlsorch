<tr>

    <td>

        <select name="pages[{{ $data['index'] }}][parent_id]" class="form-control">

            <option value="0"> -- Make Parent --</option>

            @if ($data['pages']->count() > 0)
                @foreach($data['pages'] as $page)
                    <option value="{{ $page->id }}">{!! $page->title_en !!}</option>
                @endforeach
            @endif

        </select>

    </td>

    <td>

        <select name="pages[{{ $data['index'] }}][page_id]" class="form-control">

            @if ($data['pages']->count() > 0)
                @foreach($data['pages'] as $page)
                    <option value="{{ $page->id }}">{!! $page->title_en !!}</option>
                @endforeach
            @else
                <option value="0">-- No Pages Added --</option>
            @endif

        </select>

    </td>

    <td>
        <input type="text" name="pages[{{ $data['index'] }}][page_order]" class="form-control">
    </td>

    <td class="text-left">

        <span class="btn btn-sm btn-danger btn-recover" onclick="return removeRow(this);">
            <i class="icon icon-remove"></i></span>

    </td>

</tr>