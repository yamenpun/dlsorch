@extends('admin.layouts.master')

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
                </li>

                <li>
                    <a href="{{ route($base_route) }}">{{ trans($trans_path.'general.content.page') }}</a>
                </li>

                <li class="active">{{ trans($trans_path.'general.content.add') }}</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    {{ trans($trans_path.'general.content.page-manager') }}

                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'general.content.add') }}
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <!-- PAGE CONTENT BEGINS -->

                    {!! Form::open([
                    'route' => $base_route.'.store',
                    'method' => 'post',
                    'class' => 'form-horizontal',
                    'role' => "form",
                    'enctype' => "multipart/form-data"
                    ]) !!}

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="title"> {{ trans($trans_path.'general.column.title') }} </label>

                        <div class="col-sm-9">

                            {!! Form::text('title', null, [
                               "'kl_virtual_keyboard_secure_input" => "on",
                               'id' => 'title',
                               "placeholder" => "Title",
                               "class" => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'title') !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="position"> {{ trans($trans_path.'general.column.position') }} </label>

                        <div class="col-sm-9">

                            <select name="position" class="col-xs-10 col-sm-12">

                                @foreach(config('broadway.menu') as $key => $position)
                                    <option value="{{ $key }}" {!! old('position') == $key?'selected=selected':'' !!}>{{ $position }}</option>
                                @endforeach

                            </select>

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right"
                               for="order"> {{ trans($trans_path.'general.column.order') }} </label>

                        <div class="col-sm-9">

                            {!! Form::text('order', null, [
                               "'kl_virtual_keyboard_secure_input" => "on",
                               'id' => 'order',
                               "placeholder" => "Order",
                               "class" => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'order') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="status"> {{ trans($trans_path.'general.column.status') }} </label>

                        <div class="col-sm-9">

                            <div class="control-group">

                                <div class="radio">

                                    <label>
                                        {!! Form::radio('status', 1, true, [
                                            'class' => 'ace'
                                        ]) !!}
                                        <span class="lbl">Active</span>
                                    </label>

                                </div>

                                <div class="radio">

                                    <label>
                                        {!! Form::radio('status', 0, false, [
                                             'class' => 'ace'
                                         ]) !!}
                                        <span class="lbl"> Inactive </span>
                                    </label>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="page"> {{ trans($trans_path.'general.column.page') }} </label>

                        <div class="col-sm-9">

                            <table id="sample-table-1" class="table table-striped table-bordered table-hover">

                                <thead>

                                    <tr>

                                        <th>{{ trans($trans_path.'general.column.parent-page') }}</th>

                                        <th>{{ trans($trans_path.'general.column.select-page') }}</th>

                                        <th>{{ trans($trans_path.'general.column.order') }}</th>

                                        <th>{{ trans($trans_path.'general.column.action') }}</th>

                                    </tr>

                                </thead>

                                <tbody id="row-wrapper">

                                    <tr>

                                        <td>

                                            <select name="pages[0][parent_id]" class="form-control">

                                                <option value="0"> -- Make Parent --</option>

                                                @if ($data['pages']->count() > 0)
                                                    @foreach($data['pages'] as $page)
                                                        <option value="{{ $page->id }}">{!! $page->title_en !!} @if($page->page_type == 'link-page') {{ '(Link Page)'}}@endif</option>
                                                    @endforeach
                                                @endif

                                            </select>

                                        </td>

                                        <td>

                                            <select name="pages[0][page_id]" class="form-control">

                                                @if ($data['pages']->count() > 0)
                                                    @foreach($data['pages'] as $page)
                                                        <option value="{{ $page->id }}">{!! $page->title_en !!} @if($page->page_type == 'link-page') {{ '(Link Page)'}}@endif</option>
                                                    @endforeach
                                                @else
                                                    <option value="0">-- No Pages Added --</option>
                                                @endif

                                            </select>

                                        </td>

                                        <td><input type="text" name="pages[0][page_order]" class="form-control"></td>

                                        <td class="text-left">
                                            <span class="btn btn-sm btn-danger btn-recover" onclick="return removeRow(this);"><i class="icon icon-remove"></i></span>
                                        </td>

                                    </tr>

                                </tbody>

                                <tfoot>

                                    <tr>

                                        <td colspan="3">&nbsp;</td>

                                        <td class="text-left pull-right">

                                            <a href="#" class="btn btn-primary btn-recover add_field_button"><i class="icon icon-plus"></i></a>

                                        </td>

                                    </tr>

                                </tfoot>

                            </table>

                            <script>
                                $(document).ready(function () {

                                    row_limit = 5;

                                    add_button_attr = $(".add_field_button"); //Add button ID

                                    var wrapper_attr = $("#row-wrapper");

                                    row_index_attr = parseInt(1);

                                    limit_counter = 1;


                                    $(add_button_attr).click(function (e) { //on add input button click
                                        e.preventDefault();

                                        // Load by Ajax
                                        loadRow(wrapper_attr, row_index_attr);

                                        row_index_attr++;
                                        limit_counter++;

                                        if (limit_counter >= row_limit) {
                                            add_button_attr.hide();
                                            return false;
                                        }

                                    });

                                });


                                function loadRow(wrapper_attr, index) {

                                    $.ajax({
                                        method: 'GET',
                                        url: '{{ url('admin/menu/page-html') }}' + '/' + index,
                                        error: function (request, status, error) {
                                            console.log(request.responseText);
                                        },
                                        success: function (data) {

                                            var data = $.parseJSON(data);

                                            if (data.html)
                                                wrapper_attr.append(data.html);
                                        }
                                    });
                                }

                                function removeRow(rmv_btn) {
                                    rmv_btn.parentNode.parentNode.remove();
                                    if (limit_counter <= row_limit) {
                                        limit_counter--;
                                        add_button_attr.show();
                                    }
                                }


                            </script>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="clearfix form-actions">

                <div class="col-md-offset-3 col-md-9">

                    <button class="btn btn-primary" type="submit">
                        <i class="icon-ok bigger-110"></i>
                        {{ trans($trans_path.'general.button.add-submit') }}
                    </button>

                    &nbsp; &nbsp; &nbsp;

                    <button class="btn btn-primary" type="reset">
                        <i class="icon-undo bigger-110"></i>
                        {{ trans($trans_path.'general.button.reset') }}
                    </button>

                </div>

            </div>

            {!! Form::close() !!}

        </div>

    </div>

@endsection
