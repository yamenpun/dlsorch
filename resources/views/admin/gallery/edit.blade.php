@extends('admin.layouts.master')

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
                </li>

                <li>
                    <a href="{{ route($base_route) }}">{{ trans($trans_path.'general.content.page') }}</a>
                </li>

                <li class="active">{{ trans($trans_path.'general.content.update') }}</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    {{ trans($trans_path.'general.content.page-manager') }}

                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'general.content.update') }}
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="">

                    <!-- PAGE CONTENT BEGINS -->

                    {!! Form::model($data['row'], [
                    'route' => [$base_route.'.update', $data['row']->id],
                    'method' => 'post',
                    'class' => 'form-horizontal',
                    'role' => "form",
                    'enctype' => "multipart/form-data"
                    ]) !!}

                    <input type="hidden" name="id" value="{{ $data['row']->id }}">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    <div class="">

                        <div class="tabbable tabs-left">

                            <ul class="nav nav-tabs" id="myTab3">

                                <li class="active">
                                    <a data-toggle="tab" href="#data">
                                        Data
                                    </a>
                                </li>

                                <li class="">
                                    <a data-toggle="tab" href="#general">
                                        General
                                    </a>
                                </li>

                            </ul>

                            <div class="tab-content">

                                <div id="data" class="tab-pane active">

                                    <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">

                                        <li class="active">
                                            <a data-toggle="tab" href="#nepali_tab"><img
                                                        src="{{ asset('assets/admin/ne.gif') }}"> नेपाली</a>
                                        </li>

                                        <li class="">
                                            <a data-toggle="tab" href="#english_tab"><img
                                                        src="{{ asset('assets/admin/en.jpg') }}"> English</a>
                                        </li>

                                    </ul>

                                    <div class="tab-content">

                                        <!-- Nepali Tab Start -->

                                        <div id="nepali_tab" class="tab-pane active">

                                            <div class="form-group">

                                                <label class="col-sm-2 control-label no-padding-right"
                                                       for="title_np"> {{ trans($trans_path.'general.column.title_np') }} </label>

                                                <div class="col-sm-9">

                                                    {!! Form::text('title_np', null, [
                                                       "'kl_virtual_keyboard_secure_input" => "on",
                                                       'id' => 'title_np',
                                                       "placeholder" => "शिर्षक",
                                                       "class" => "col-xs-10 col-sm-12",
                                                    ]) !!}

                                                    {!! AppHelper::getValidationErrorMsg($errors, 'title_np') !!}

                                                </div>

                                            </div>

                                            <div class="space-4"></div>

                                            <div class="form-group">

                                                <label class="col-sm-2 control-label no-padding-right"
                                                       for="description_np"> {{ trans($trans_path.'general.column.description_np') }} </label>

                                                <div class="col-sm-9">

                                                    {!! Form::textarea('description_np', null, [
                                                       "'kl_virtual_keyboard_secure_input" => "on",
                                                       'id' => 'description_np',
                                                       "class" => "col-xs-10 col-sm-12 form-control mceEditor",
                                                    ]) !!}

                                                    {!! AppHelper::getValidationErrorMsg($errors, 'description_np') !!}

                                                </div>

                                            </div>

                                        </div>

                                        <!-- English Tab Start -->

                                        <div id="english_tab" class="tab-pane">

                                            <div class="form-group">

                                                <label class="col-sm-2 control-label no-padding-right"
                                                       for="title_en"> {{ trans($trans_path.'general.column.title_en') }} </label>

                                                <div class="col-sm-9">

                                                    {!! Form::text('title_en', null, [
                                                       "'kl_virtual_keyboard_secure_input" => "on",
                                                       'id' => 'title_en',
                                                       "placeholder" => "Title",
                                                       "class" => "col-xs-10 col-sm-12",
                                                    ]) !!}

                                                    {!! AppHelper::getValidationErrorMsg($errors, 'title_en') !!}

                                                </div>

                                            </div>

                                            <div class="space-4"></div>

                                            <div class="form-group">

                                                <label class="col-sm-2 control-label no-padding-right"
                                                       for="description_en"> {{ trans($trans_path.'general.column.description_en') }} </label>

                                                <div class="col-sm-9">

                                                    {!! Form::textarea('description_en', null, [
                                                       "'kl_virtual_keyboard_secure_input" => "on",
                                                       'id' => 'description_en',
                                                       "class" => "col-xs-10 col-sm-12 form-control mceEditor",
                                                    ]) !!}

                                                    {!! AppHelper::getValidationErrorMsg($errors, 'description_en') !!}

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <!-- General Tab Content -->

                                <div id="general" class="tab-pane">

                                    @if (isset($data['row']))

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right" for="file">Existing Image</label>

                                        <div class="col-sm-9">

                                            @if ($data['row']->feature_image !== '')
                                                <img src="{{ asset(config('broadway.url.frontend.image').'/gallery/'.$data['row']->feature_image) }}"
                                                     alt="{{ $data['row']->title_en }} "
                                                     style="max-width: 200px; max-height:50px">
                                            @else
                                                <strong>{{ trans('general.common.no-image-found') }}</strong>
                                            @endif

                                        </div>

                                    </div>

                                    @endif

                                    <div class="space-4"></div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right"
                                               for="file"> {{ trans($trans_path.'general.column.image') }} </label>

                                        <div class="col-sm-9">

                                            {!! Form::file('file', null, true, [
                                               "'kl_virtual_keyboard_secure_input" => "on",
                                               'id' => 'file',
                                               "class" => "col-xs-10 col-sm-12",
                                            ]) !!}

                                            {!! AppHelper::getValidationErrorMsg($errors, 'file') !!}

                                        </div>

                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right"
                                               for="order"> {{ trans($trans_path.'general.column.order') }} </label>

                                        <div class="col-sm-9">

                                            {!! Form::text('order', null, [
                                               "'kl_virtual_keyboard_secure_input" => "on",
                                               'id' => 'order',
                                               "placeholder" => "Order",
                                               "class" => "col-xs-10 col-sm-12",
                                            ]) !!}

                                            {!! AppHelper::getValidationErrorMsg($errors, 'order') !!}

                                        </div>

                                    </div>

                                    <div class="space-4"></div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label no-padding-right"
                                               for="status"> {{ trans($trans_path.'general.column.status') }} </label>

                                        <div class="col-sm-9">

                                            <div class="control-group">

                                                <div class="radio">
                                                    <label>
                                                        {!! Form::radio('status', 1, true, [
                                                            'class' => 'ace'
                                                        ]) !!}
                                                        <span class="lbl">Active</span>
                                                    </label>
                                                </div>

                                                <div class="radio">
                                                    <label>
                                                        {!! Form::radio('status', 0, false, [
                                                             'class' => 'ace'
                                                         ]) !!}
                                                        <span class="lbl"> Inactive </span>
                                                    </label>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="clearfix form-actions">

                        <div class="col-md-offset-3 col-md-9">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                {{ trans($trans_path.'general.button.update-submit') }}
                            </button>

                            &nbsp; &nbsp; &nbsp;

                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                {{ trans($trans_path.'general.button.reset') }}
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

@endsection

@section('page_specific_scripts')
    @include('admin.common.editor')
@endsection