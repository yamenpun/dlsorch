@extends('admin.layouts.master')

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
                </li>

                <li class="active">{{ trans($trans_path.'general.content.page') }}</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    {{ trans($trans_path.'general.content.page-manager') }}

                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'general.content.list') }}
                    </small>

                    <a href="{{ route($base_route.'.add') }}" class="btn btn-primary btn-sm">
                        <i class="icon-plus-sign bigger-110"></i>
                        {{ trans($trans_path.'general.button.add') }}
                    </a>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">

                        <div class="col-xs-12">

                            <div class="table-responsive">

                                @if(isset($errors) && count($errors) > 0)
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {{ $errors->first('message') }}
                                        <br>
                                    </div>
                                @endif

                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">

                                    <thead>

                                        <tr>

                                            <th style="width: 20px;" class="center">
                                                <label>
                                                    <input class="ace" type="checkbox">
                                                    <span class="lbl"></span>
                                                </label>
                                            </th>

                                            <th style="width: 20px;">{{ trans($trans_path.'general.column.title_en') }}</th>

                                            <th style="width: 20px;">{{ trans($trans_path.'general.column.description_en') }}</th>

                                            <th style="width: 20px;">{{ trans($trans_path.'general.column.image') }}</th>

                                            <th style="width: 20px;">{{ trans($trans_path.'general.column.url') }}</th>

                                            <th style="width: 20px;">{{ trans($trans_path.'general.column.order') }}</th>

                                            <th style="width: 20px;">{{ trans($trans_path.'general.column.status') }}</th>

                                            <th style="width: 20px;">{{ trans($trans_path.'general.column.action') }}</th>

                                        </tr>

                                    </thead>

                                    <tbody>

                                    @if ($data['rows']->count() > 0)
                                        @foreach($data['rows']->getCollection() as $row)

                                            <tr>

                                                <td class="center">
                                                    <label>
                                                        <input class="ace" type="checkbox">
                                                        <span class="lbl"></span>
                                                    </label>
                                                </td>

                                                <td>{{ $row->title_np }} <br> {{ $row->title_en }} </td>

                                                <td>
                                                    @if($row->description_np || $row->description_en)
                                                        {{ str_limit($row->description_np, 200) }} <br/> {{ str_limit($row->description_en, 200) }}
                                                    @else
                                                        <p>{{ trans('general.common.no-description-found') }}</p>
                                                    @endif
                                                </td>

                                                <td>
                                                    @if($row->feature_image)
                                                        <img src="{{ asset(config('broadway.url.frontend.image').'gallery/'.$row->feature_image) }}"
                                                             alt="{{ $row->title_en }}"
                                                             style="max-width: 200px; max-height:50px">
                                                    @else
                                                        <p>{{ trans('general.common.no-image-found') }}</p>
                                                    @endif
                                                </td>

                                                <td>{{ $row->url }}</td>

                                                <td>{{ $row->order }}</td>

                                                <td>
                                                    @if($row->status == 1)
                                                        <button class="btn btn-minier btn-primary">Active</button>
                                                    @else
                                                        <button class="btn btn-minier btn-yellow">Inactive</button>
                                                    @endif
                                                </td>

                                                <td>

                                                    <div class="btn-group">

                                                        <a href="{{ route($base_route.'.edit', ['id' => $row->id]) }}">
                                                            <button class="btn btn-xs btn-info">
                                                                <i class="icon-edit bigger-120"></i>
                                                            </button>
                                                        </a>

                                                        <a href="{{ route($base_route.'.delete', ['id' => $row->id]) }}">
                                                            <button class="btn btn-xs btn-danger deleteConfirm">
                                                                <i class="icon-trash bigger-120"></i>
                                                            </button>
                                                        </a>

                                                    </div>

                                                </td>

                                            </tr>

                                        @endforeach

                                        <tr>
                                            <td colspan="8">{{ $data['rows']->links() }}</td>
                                        </tr>

                                    @else

                                        <tr>
                                            <td colspan="8">{{ trans('general.common.no-data-found') }}</td>
                                        </tr>

                                    @endif

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                    <div class="hr hr-18 dotted hr-double"></div>

                </div>

            </div>

        </div>

    </div>

@endsection