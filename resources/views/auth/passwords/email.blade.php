@extends('layouts.app')

@section('content')

    <div class="position-relative">
        <div id="login-box" class="login-box visible widget-box no-border">
            <div class="widget-body">
                <div class="widget-main">
                    <h4 class="header blue lighter bigger">
                        <i class="icon-coffee red"></i>
                        Reset your password
                    </h4>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="space-6"></div>

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">

                        {{ csrf_field() }}

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <fieldset>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address"/>
                                    <i class="icon-user"></i>
                                </span>
                            </label>

                            <div class="space"></div>

                            <div class="clearfix">

                                <button type="submit" class="pull-right btn btn-sm btn-primary">
                                    <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                                </button>
                            </div>

                            <div class="space-4"></div>
                        </fieldset>
                    </form>

                </div><!-- /widget-main -->

                <div class="toolbar clearfix">
                    <div>
                        <a href="{{ url('login') }}" class="forgot-password-link">
                            <i class="icon-arrow-left"></i>
                            Go back to login
                        </a>
                    </div>
                </div>
            </div><!-- /widget-body -->
        </div><!-- /login-box -->

@endsection