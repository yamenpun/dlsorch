@extends('layouts.app')

@section('content')

    <div class="position-relative">
        <div id="login-box" class="login-box visible widget-box no-border">
            <div class="widget-body">
                <div class="widget-main">
                    <h4 class="header blue lighter bigger">
                        <i class="icon-coffee green"></i>
                        Please Enter Your Information
                    </h4>

                    <div class="space-6"></div>

                    <form method="POST" action="{{ url('/login') }}">

                        {!! csrf_field() !!}

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <fieldset>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address"/>
                                    <i class="icon-user"></i>
                                </span>
                            </label>

                            <div class="space"></div>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="password" class="form-control" name="password" placeholder="Password"/>
                                    <i class="icon-lock"></i>
                                </span>
                            </label>

                            <div class="space"></div>

                            <div class="clearfix">
                                <label class="inline">
                                    <input type="checkbox" name="remember" class="ace"/>
                                    <span class="lbl"> Remember Me</span>
                                </label>

                                <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                    <i class="icon-key"></i>
                                    Login
                                </button>
                            </div>

                            <div class="space-4"></div>
                        </fieldset>
                    </form>

                </div><!-- /widget-main -->

                <div class="toolbar clearfix">
                    <div>
                        <a href="{{ url('/password/reset') }}" class="forgot-password-link">
                            <i class="icon-arrow-left"></i>
                            I forgot my password
                        </a>
                    </div>
                </div>
            </div><!-- /widget-body -->
        </div><!-- /login-box -->

@endsection