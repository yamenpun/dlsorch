@extends('frontend.layouts.master_frontend')

@section('content')

    <div class="centerbody">

        <div class="press-release1">
            @foreach($composer_introduction as $introduction)
                <h2>{{ AppHelper::getDataByLang($introduction, 'title') }}</h2>

                <div class="news-box">
                    <ul>
                        <br>
                        <div>{!! AppHelper::getDataByLang($introduction, 'description') !!}</div>
                        <br>
                    </ul>
                </div>
            @endforeach
        </div>
    </div>
    <div class="right-content">
        @include('frontend.includes.staff')

        @include('frontend.includes.resources')

        @include('frontend.includes.gallery')
    </div>

@stop