@extends('frontend.layouts.master_frontend')

@section('content')

    <div class="innerbody">

        <div class="clear"></div>

        <div class="press-release1">

            <h2>{{ trans('general.heading.staff') }}</h2>

            <div class="news-box">

                <table width="100%" class="table">

                    <tbody>

                    @if($composer_staffs->count() > 0)

                        @foreach($composer_staffs as $staff)

                            <tr bgcolor="{{ ($staff->id % 2 == 0)? '#dddddd': '#eeeedd' }}">

                                <td width="141"><p><b>{{ trans('general.heading.fullname') }}</b></p></td>

                                <td width="455" align="left"><p>{{ AppHelper::getDataByLang($staff, 'fullname') }}</p>
                                </td>

                                <td width="118" valign="middle" align="center" rowspan="5">
                                    @if($staff->image)
                                        <img width="100" vspace="5" hspace="5" height="100" align="right"
                                             src="{{ asset(config('broadway.url.frontend.image').'staff/'.$staff->image) }}">
                                    @endif
                                </td>

                            </tr>

                            <tr bgcolor="{{ ($staff->id % 2 == 0)? '#dddddd': '#eeeedd' }}">

                                <td><p><b>{{ trans('general.heading.designation') }}</b></p></td>

                                <td align="left"><p>{{ AppHelper::getDataByLang($staff, 'designation') }}</p></td>

                            </tr>

                            <tr bgcolor="{{ ($staff->id % 2 == 0)? '#dddddd': '#eeeedd' }}">

                                <td><p><b>{{ trans('general.heading.division') }}</b></p></td>

                                <td align="left"><p>{{ AppHelper::getDataByLang($staff, 'division') }}</p></td>

                            </tr>

                            <tr bgcolor="{{ ($staff->id % 2 == 0)? '#dddddd': '#eeeedd' }}">

                                <td><p><b>{{ trans('general.heading.email') }}</b></p></td>

                                <td align="left"><p>{{ $staff->email }}</p></td>

                            </tr>

                            <tr></tr>

                            <tr></tr>

                        @endforeach

                    @else
                        <tr>
                            <td><p>{{ trans('general.heading.no-data-found') }}</p></td>
                        </tr>
                    @endif

                    </tbody>

                </table>

            </div>

        </div>

    </div>

@stop