@extends('frontend.layouts.master_frontend')

@section('content')

    <div class="innerbody" xmlns="http://www.w3.org/1999/html">

        <div class="clear"></div>

        <div class="press-release1">

            <h2>{{ trans('general.heading.gallery') }}</h2>

            @if($composer_galleries->count() > 0)

                @foreach($composer_galleries as $gallery)

                    <div class="news-box">

                        <br>

                        <ol class="bor">

                            <li>

                                <b>
                                    <a href="{{ route('gallery.view', ['id' => $gallery->id]) }}">{{ AppHelper::getDataByLang($gallery, 'title') }}</a>
                                </b>

                                <br>

                                <small>{{ date('Y-m-d', strtotime($gallery->created_at)) }}</small>

                                <br><br>

                                <p>
                                    <img src="{{ asset(config('broadway.url.frontend.image').'gallery/'.$gallery->feature_image) }}"
                                         alt="{{ AppHelper::getDataByLang($gallery, 'title') }}"
                                         style="max-width: 300px; max-height:100px"></p>
                                @if(AppHelper::getDataByLang($gallery, 'description'))
                                    <p>{!! AppHelper::getDataByLang($gallery, 'description') !!}</p>
                                @endif
                                [<a href="{{ route('gallery.view', ['id' => $gallery->id]) }}">+ {{ trans('general.heading.more')}} </a>]
                                <br><br>

                            </li>

                        </ol>

                    </div>

                @endforeach

            @else
                <br>
                <p>{{ trans('general.heading.no-data-found') }}</p>
            @endif

        </div>

    </div>

@stop