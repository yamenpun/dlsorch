@extends('frontend.layouts.master_frontend')

@section('content')

    <div class="innerbody">

        <div class="clear"></div>

        <div class="press-release1">

            <h2>{{ trans('general.heading.gallery') }}</h2>

            @if($composer_gallery_images_home->count() > 0)

                @foreach($composer_gallery_images_home as $images)

                    @if($images->gallery_id == $data['id'])

                        <div id="GalleryClip">

                            <a href="{{ route('gallery.image', ['id' => $images->id]) }}">
                                <br>
                                <img src="{{ asset(config('broadway.url.frontend.image').'gallery/'.'100_100_'.$images->image) }}">
                                <br>
                                <p>{{ $images->caption_en }}</p>
                            </a>

                        </div>

                    @endif

                @endforeach

            @else
                <br>
                <p>{{  trans('general.heading.no-data-found') }}</p>
            @endif

        </div>

    </div>

@stop