@extends('frontend.layouts.master_frontend')

@section('content')

    <div class="innerbody">

        <div class="clear"></div>

        <div class="press-release1">

            <h2>{{ AppHelper::getDataBylang($data['row'], 'title') }}</h2>
            <br>
            <small>{{ trans('general.heading.published-date') }}
                : {{ date('Y-m-d', strtotime($data['row']->created_at)) }}</small>
            <br>
            @foreach($composer_users as $user)
                @if($data['row']->user_id == $user->id)
                    <small>{{ trans('general.heading.published-by') }}
                        : {!! AppHelper::getDataByLang($user, 'fullname') !!}</small>
                @endif
            @endforeach
            <br><br>
            @if($data['row']->image)
                <img width="250" height="200" src="{{ asset(config('broadway.url.frontend.image').'notice/'.$data['row']->image) }}"
                     alt="{{ $data['row']->title_en }}" style="max-width: 500px; max-height:200px">
                <p><a href="{{ route('notice.docs.view', ['id' => $data['row']->id]) }}">Click here to enlarge the image</a></p>
            @endif
            <div>{!! AppHelper::getDataByLang($data['row'], 'description') !!}</div>
            <br>

        </div>
    </div>

@stop