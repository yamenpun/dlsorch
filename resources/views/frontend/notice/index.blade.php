@extends('frontend.layouts.master_frontend')

@section('content')

    <div class="innerbody">

        <div class="clear"></div>

        <div class="press-release1">
            <h2>{{ trans('general.heading.notice') }}</h2>
            <div class="news-box">
                <br>
                @if($composer_notices->count() > 0)
                    @foreach($composer_notices as $notice)
                        <ol class="bor">
                            <li>
                                <b><a href="{{ route('notice.view', ['id' => $notice->id]) }}">{{ AppHelper::getDataByLang($notice, 'title') }}</a></b><br>
                                <small>{{ date('Y-m-d', strtotime($notice->created_at)) }}</small>
                                <br><br>
                                [<a href="{{ route('notice.view', ['id' => $notice->id]) }}">+ {{ trans('general.heading.more') }}</a>
                                ]
                            </li>
                        </ol>
                    @endforeach
                @else
                    <p>{{ trans('general.heading.no-data-found') }}</p>
                @endif
            </div>
        </div>

    </div>

@stop