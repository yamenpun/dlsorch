@extends('frontend.layouts.master_frontend')

@section('content')

    <div class="innerbody">

        <div class="press-release1">
            @foreach($data['row'] as $item)
                <h2>{{ AppHelper::getDataByLang($item, 'title') }}</h2>

                <div class="news-box">

                    <br>
                    @if($item->image)
                        <img src="{{ asset(config('broadway.url.frontend.image').'page/'.$item->image) }}"
                             alt="{{ AppHelper::getDataByLang($item, 'title') }}"
                             style="max-width: 300px; max-height:100px">
                    @endif
                    @if(AppHelper::getDataByLang($item, 'content'))
                        <div>{!! AppHelper::getDataByLang($item, 'content') !!}</div>
                    @endif
                    <br>

                </div>
            @endforeach
        </div>
    </div>
@stop