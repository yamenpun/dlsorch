@extends('frontend.layouts.master_frontend')

@section('content')

    <div class="innerbody">

        <div class="clear"></div>

        <div class="press-release1">

            <h2>{{ trans('general.heading.contact-title') }}</h2>

            <h4 style="font-size:16px; margin-top:10px; padding: 10px 0;">{{ trans('general.heading.office') }}</h4>

            @foreach($composer_contact as $contact)
                <p style="font-size:13px;">{{ AppHelper::getDataByLang($contact, 'address') }}</p>
                <p style="font-size:13px;">{{ trans('general.heading.phone') }} :- {{ $contact->phone_no }} </p>
                <p style="font-size:13px;">{{ trans('general.heading.fax') }} :- {{ $contact->fax_no }}</p>
                <p style="font-size:13px;">{{ trans('general.heading.post') }} :- {{ $contact->post_box_no }}</p>
                <p style="font-size:13px;">{{ trans('general.heading.email') }} :- {{ $contact->email }}</p>
                <h4 style="font-size:16px; margin-top:10px; padding: 10px 0;">{{ AppHelper::getDataByLang($contact, 'contact_person_post') }}</h4>
                <p style="font-size:13px;">{{ AppHelper::getDataByLang($contact, 'contact_person_name') }}</p>
            @endforeach
        </div>

    </div>
@stop