<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content=""/>
    <meta name="keyphrases" content="Office of Prime Minister and Council Minister"/>
    <meta name="description" content="Office of Prime Minister and Council Minister"/>
    <meta name="google-site-verification" content="kAWpuC2wr6CAl43StK3jcwA3R2rSr5kK5E0j7PxvGos"/>

    <title> DLSO | Dev </title>

    <link href="{{ asset('assets/frontend/assets/css/reset.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/frontend/assets/css/style.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/frontend/assets/js/superfish/css/superfish.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/frontend/assets/js/superfish/css/superfish1.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/frontend/assets/js/superfish/css/superfish-vertical.css') }}" rel="stylesheet"/>


    <style>
        #news_btn a {
            color: #fff;
            display: block;
            padding: 12px;
            margin-bottom: 4px;
            background: #1A4A2A;
            font-size: 18px;
            border: 1px solid #888;
        }

        #news_btn a:hover {
            color: #fff;
            background: #990000;
        }
    </style>

    <script type="text/javascript" src="{{ asset('assets/frontend/assets/js/jquery-1.3.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/frontend/assets/js/ui/jquery-ui-1.7.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/frontend/assets/js/jcarousellite_1.0.1.pack.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/frontend/assets/js/jquery.cycle.all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/frontend/assets/js/superfish/js/superfish.js') }}"></script>

    <script type="text/javascript">
        jQuery(function () {
            jQuery('.photobox').cycle({
                fx: 'fade',  // choose your transition type, ex: fade, scrollUp, shuffle, etc...
                slideExpr: 'li'
            });

            jQuery("ul.sf-menu").superfish({
                animation: {height: 'show'},   // slide-down effect without fade-in
                delay: 150               // 1.2 second delay on mouseout
            });

            jQuery("ul.sf-menu-1").superfish({
                animation: {height: 'show'},   // slide-down effect without fade-in
                delay: 150               // 1.2 second delay on mouseout
            });

            jQuery('.slideshow').cycle({
                fx: 'fade',
                slideExpr: '.slide',
                next: '.nxt',
                prev: '.prev'
            });

            $('#slide-animation').fadeIn();

        });
    </script>
</head>

<body>
<div class="wrapper">

    @include('frontend.includes.header')

    <div class="clear"></div>

    @include('frontend.includes.highlights')

    <div class="bodypart">

        <div class="leftpart">

            @include('frontend.includes.left-menu')

            <p>&nbsp;</p>

            @include('frontend.includes.quick-links')

        </div>

        <div class="rightpart">

            @yield('content')

        </div>

    </div>

    <div class="clearall"></div>

    <div class="footer">

        @include('frontend.includes.footer')

    </div>
</div>
</body>
</html>


