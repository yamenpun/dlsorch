<div class="footer-left">
	<li>
		<a href="{{ route('home') }}">{{ trans('general.heading.home') }}</a> &nbsp;|
		<a href="{{ route('news') }}">{{ trans('general.heading.news') }}</a> &nbsp;|
		<a href="{{ route('notice') }}">{{ trans('general.heading.notice') }}</a> &nbsp;|
	    <a href="{{ route('feedback') }}">{{ trans('general.heading.feedback') }}</a> &nbsp;|
		<a href="{{ route('contact') }}">{{ trans('general.heading.contact') }}</a>
	</li>
</div>
<div class="footer-right">
	<b> &copy; Copyright {{ date('Y') }}.  DLSO Nepal </br> All Right Reserved. </b>
</div>