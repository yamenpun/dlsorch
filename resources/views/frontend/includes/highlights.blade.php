<div class="hightlight_block">

    <span>{{ trans('general.heading.high-lights') }} :</span>

    <marquee scrollamount="5" scrolldelay="30" behavior="scroll" onmouseover="stop('stop');" onmouseout="start('start');">

        @if($composer_notices_home->count() > 0)
            @foreach($composer_notices_home as $notice)
                <a href="{{ route('notice.view', ['id' => $notice->id]) }}">{{ AppHelper::getDataByLang($notice, 'title') }}</a>
                |&nbsp;&nbsp;&nbsp;
            @endforeach
        @else
            <p>{{ trans('general.heading.no-data-found') }}</p>
        @endif

    </marquee>

</div>  