@foreach($composer_staffs as $staff)

    <div class=pm-profile>

        @if($staff->image)

        <img src="{{ asset(config('broadway.url.frontend.image').'staff/'.$staff->image) }}" width="91" alt="" />

        @endif
        <br>

        <span>{{ AppHelper::getDataByLang($staff, 'fullname') }}</span>

        <h4>{{ AppHelper::getDataByLang($staff, 'designation') }}</h4>

        <h4>{{ AppHelper::getDataByLang($staff, 'division') }}</h4>

    </div>

@endforeach