<div id="quick-links">

    <h2>{{ trans('general.heading.useful-links') }}</h2>

    <div class="linkbox">

        <ul>
            @if($composer_links->count() > 0)
                @foreach($composer_links as $link)
                    <li><a href="{{ $link->url }}" target="_blank">{{ AppHelper::getDataByLang($link, 'title') }}</a>
                    </li>
                @endforeach
            @else
                <br>
                <li>{{ trans('general.heading.no-data-found') }}</li>
            @endif
        </ul>

    </div>

</div>