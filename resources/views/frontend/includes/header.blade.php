<div class="header">

    <div class="logo">
        <h1>
            <a href="{{ route('home') }}">
                <img src="{{ asset(config('broadway.url.frontend.image').'logo-'. session()->get('app_language') .'.png') }}"/>
            </a>
        </h1>
    </div>

    <div class="flag"><img src="{{ asset(config('broadway.url.frontend.image').'flag.gif') }}" border="0" alt=""/></div>

    <div class="top-nav">

        <ul class="sf-menu">

            <li><a href="{{ route('home') }}">{{ trans('general.heading.home') }}</a></li>

            <li><a href="{{ route('news') }}">{{ trans('general.heading.news') }}</a></li>

            <li><a href="{{ route('notice') }}">{{ trans('general.heading.notice') }}</a></li>

            <li><a href="{{ route('staff') }}">{{ trans('general.heading.staff') }}</a></li>

            @foreach($composer_header_menus as $item)

                @if ($item->parent_id == 0)

                    <li>
                        @if($item->page_type == 'content-page')
                            <a href="{{ route('page', ['url'=>$item->url]) }}">{{ AppHelper::getDataByLang($item, 'title') }}</a>
                        @else
                            <a href="{{ $item->url }}" target="_blank">{{ AppHelper::getDataByLang($item, 'title') }}</a>
                        @endif
                        {!! AppHelper::subMenu($composer_header_menus, $item->page_id) !!}
                    </li>

                @endif

            @endforeach

            <li><a href="{{ route('gallery') }}">{{ trans('general.heading.gallery') }}</a></li>

            <li><a href="{{ route('feedback') }}">{{ trans('general.heading.feedback') }}</a></li>

            <li><a href="{{ route('contact') }}">{{ trans('general.heading.contact') }}</a></li>

        </ul>

        <div class="search">
            <a href="{{ url()->current() }}?lan=np" title="Nepali"> नेपाली </a> |
            <a href="{{ url()->current() }}?lan=en" title="English"> English </a>
        </div>

    </div>

</div>