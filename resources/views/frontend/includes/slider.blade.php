<div class="animation">

    <div class="banner">

        <div class="slideshow" id="slide-animation" style="display: none;">

            @if($composer_sliders->count() > 0)
                @foreach($composer_sliders as $slider)
                    <div class="slide">
                        <div class="caption">{{ AppHelper::getDataByLang($slider, 'caption') }}</div>
                        <img src="{{ asset(config('broadway.url.frontend.image').'slider/'.$slider->image) }}"
                             alt="{{ AppHelper::getDataByLang($slider, 'caption') }}" width="450" height="270"/>
                    </div>
                @endforeach
            @else
                <br>
                <p>{{ trans('general.heading.no-data-found') }}</p>
            @endif

        </div>

    </div>

</div>