<div id="nav">

    <ul class="sf-menu-1 sf-vertical">

        <li><a href="{{ route('introduction') }}">{{ trans('general.heading.introduction') }}</a></li>

        @foreach($composer_left_menus as $item)

            @if ($item->parent_id == 0)

                <li>
                    <a href="{{ route('page', ['url'=>$item->url]) }}">{{ AppHelper::getDataByLang($item, 'title') }}</a>
                    {!! AppHelper::subMenu($composer_header_menus, $item->page_id) !!}
                </li>

            @endif

        @endforeach

    </ul>

</div>