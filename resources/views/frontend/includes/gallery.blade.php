<div id="photogallery">
    <h2>{{ trans('general.heading.photo-gallery') }}</h2>
    <div class="gallery-slideshow" style="border:1px solid #CCCCCC;padding-left:5px;">
        <ul>
            @if($composer_gallery_images_home->count() > 0)
                <li>
                    @foreach($composer_gallery_images_home as $images)
                        <a href="{{ route('gallery') }}"/>
                        <img src="{{ asset(config('broadway.url.frontend.image').'gallery/'.$images->image) }}" alt=""
                             width="72" height="40"/>
                        </a>
                    @endforeach
                </li>
            @else
                <br>
                <li>{{ trans('general.heading.no-data-found') }}</li>
            @endif
        </ul>
    </div>
</div>
