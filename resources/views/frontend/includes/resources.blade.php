<div id="quick-links">

    <h2>{{ trans('general.heading.latest-resources') }}</h2>

    <div class="linkbox">

        <ul>
            @if($composer_documents->count() >0)
                @foreach($composer_documents as $document)
                    <li><a href="{{ asset(config('broadway.url.frontend.image').'document/'.$document->document) }}"
                           target="_blank">{{ AppHelper::getDataByLang($document, 'title') }}</a></li>
                @endforeach
            @else
                <li>{{ trans('general.heading.no-data-found') }}</li>
            @endif
        </ul>

    </div>

</div>