@extends('frontend.layouts.master_frontend')

@section('content')

    <div class="innerbody">

        <div class="clear"></div>

        <div class="press-release1">
            <h2>{{ trans('general.heading.news') }}</h2>
            <div class="news-box">
                <br>
                @if($composer_news->count() > 0)
                    @foreach($composer_news as $news)
                        <ol class="bor">
                            <li>
                                <b><a href="{{ route('news.view', ['id' => $news->id]) }}">{{ AppHelper::getDataByLang($news, 'title') }}</a></b><br>
                                <small>{{ date('Y-m-d', strtotime($news->created_at)) }}</small>
                                <br><br>
                                [<a href="{{ route('news.view', ['id' => $news->id]) }}">+ {{ trans('general.heading.more') }}</a>
                                ]
                            </li>
                        </ol>
                    @endforeach
                @else
                    <p>{{ trans('general.heading.no-data-found') }}</p>
                @endif
            </div>
        </div>

    </div>

@stop