@extends('frontend.layouts.master_frontend')

@section('content')

    <div class="innerbody">

        <div class="clear"></div>

        <div class="press-release1">

            <h2>{{ AppHelper::getDataByLang($data['row'], 'title') }}</h2>
            <br>
            <small>{{ trans('general.heading.published-date') }}
                : {{ date('Y-m-d', strtotime($data['row']->created_at)) }}</small>
            <br>
            @foreach($composer_users as $user)
                @if($data['row']->user_id == $user->id)
                    <small>{{ trans('general.heading.published-by') }}
                        : {!! AppHelper::getDataByLang($user, 'fullname') !!}</small>
                @endif
            @endforeach
            <br><br>
            @if($data['row']->file)
                <img src="{{ asset(config('broadway.url.frontend.image').'news/'.$data['row']->file) }}"
                     alt="{{ $data['row']->title_en }}" style="max-width: 500px; max-height:200px">
            @endif
            <div>{!! AppHelper::getDataByLang($data['row'], 'description') !!}</div>
            <br>

        </div>
    </div>

@stop