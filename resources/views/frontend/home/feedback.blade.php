@extends('frontend.layouts.master_frontend')

@section('content')

    <div class="innerbody">

        <div class="clear"></div>

        <div class="press-release1">

            <h2>{{ trans('general.heading.feedback-title') }}</h2>

            <form>

                <table bordercolor="#FFFFFF" border="0">

                    <tbody>

                        <tr>
                            <td colspan="2">
                                <div id="adminmenu"></div>
                            </td>
                        </tr>

                        <tr>
                            <td><span class="style1">Name</span></td>
                        </tr>

                        <tr>
                            <td><input type="text" maxlength="100" size="40" id="name" name="name"></td>
                        </tr>

                        <tr>
                            <td><span class="style1">Email</span></td>
                        </tr>

                        <tr>
                            <td><input type="email" required maxlength="30" size="40" id="email" name="email"></td>
                        </tr>

                        <tr>
                            <td><span class="style1">Topic</span></td>
                        </tr>

                        <tr>
                            <td><input type="text" maxlength="100" size="40" id="topic" name="topic"></td>
                        </tr>
                        <tr>
                            <td><span class="style1">Feedback</span></td>
                        </tr>

                        <tr>
                            <td colspan="2" border="1"><textarea id="feedback" rows="10" cols="50"
                                                                 name="feedback"></textarea></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>

                            <td>
                                <input type="submit" value="Submit" id="btnSubmit" name="btnSubmit">
                                <input type="reset" value="Reset" id="btnReset" name="btnReset">
                            </td>

                        </tr>

                    </tbody>

                </table>

            </form>

        </div>

    </div>

@stop