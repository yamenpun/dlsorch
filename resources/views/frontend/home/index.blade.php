@extends('frontend.layouts.master_frontend')

@section('content')

    <div class="centerbody">

        @include('frontend.includes.slider')

        <div class="clear"></div>

        <div class="press-release">

            @foreach($composer_introduction as $introduction)

                <h2>{{ AppHelper::getDataByLang($introduction, 'title') }}</h2>

                <div class="news-box">

                    <ul>
                        <br>
                        <div>{!! str_limit(AppHelper::getDataByLang($introduction, 'description'), 500) !!}</div>
                        <a href="{{  route('introduction') }}">{{ trans('general.heading.read-more') }}</a>
                        <br>
                        <br>
                    </ul>
                    <br>
                </div>
            @endforeach

            <div class="news-box">

                <h2>{{ trans('general.heading.notices') }}</h2>

                <ul>
                    @if($composer_notices_home->count() > 0)
                        @foreach($composer_notices_home as $notice)
                            <li>
                                <a href="{{ route('notice.view', ['id' => $notice->id]) }}">{{ AppHelper::getDataByLang($notice, 'title') }}</a>
                            </li>
                        @endforeach
                    @else
                        <br>
                        <p>{{ trans('general.heading.no-data-found') }}</p>
                    @endif
                </ul>

            </div>
        </div>

        <div class="buttons">
            <a href="#" title="Check Mail" target="_blank">
                <img src="{{ asset(config('broadway.url.frontend.image').'check-mail.jpg') }}" width="201" height="42"
                     alt=""/>
            </a>
        </div>
    </div>
    <div class="right-content">
        @include('frontend.includes.staff')

        @include('frontend.includes.resources')

        @include('frontend.includes.gallery')
    </div>

@stop