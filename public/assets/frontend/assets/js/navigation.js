jQuery(function() {
    //setup - all values in pixels
    var navWidth = 180;
    var ulIndent = 15;
    //end setup

    var pathname = window.location.pathname;

    jQuery('#nav').css('width', navWidth + 'px');
    jQuery('#nav ul').css('width', navWidth + 'px');
    jQuery('#nav ul').css('margin-left', ulIndent + 'px');

    jQuery('#nav a').each(function() {
        var level = jQuery(this).parents('ul').length;
        var liWidth = navWidth - (ulIndent * level) + 30;
        jQuery(this).parent('li').css('width', liWidth + 'px');
    });

    //prepend expand/collapse icons
    jQuery('#nav li').each(function() {
        if ($(this).children('ul').length > 0) {
            if ($(this).children('ul').is(":visible")) {
                $(this).prepend('<img src="images/imgOnOpen.gif" />');
            }
            else {
                $(this).prepend('<img src="images/imgOffClosed.gif" />');
            }
        }
    });

    //Required Section
    var slideSpeed = 'slow'; // 'slow', 'normal', 'fast', or miliseconds 
    jQuery('#nav a').each(function() {
        var thisHref = $(this).attr('href')
        if ((window.location.pathname.indexOf(thisHref) == 0) || (window.location.pathname.indexOf('/' + thisHref) == 0)) {
            $(this).addClass('Current');
        }
    });
    
    jQuery('.Current').parent('li').children('ul').show();
    jQuery('.Current').parents('ul').show();

    //prepend expand/collapse icons
    jQuery('#nav li').each(function() {
        if (jQuery(this).children('ul').length > 0) {
            if (jQuery(this).children('ul').is(":visible")) {
                jQuery(this).children('img').attr('src', 'images/imgOnOpen.gif');
            }
        }
    });
    
    jQuery('#nav img').click(function() {
        if (jQuery(this).parent('li').children('ul').html() != null) {
            jQuery(this).parent('li').parent('ul').children('li').children('ul').hide(slideSpeed);
            jQuery(this).parent('li').parent('ul').children('li').children('img').attr('src', 'images/imgOffClosed.gif');
            jQuery(this).delay(100).is(':hidden');
            if (jQuery(this).parent('li').children('ul').css('display') == "block") {
                jQuery(this).parent('li').children('ul').hide(slideSpeed);
                jQuery(this).attr('src', 'images/imgOffClosed.gif');
            } else {
                jQuery(this).parent('li').children('ul').show(slideSpeed);
                jQuery(this).attr('src', 'images/imgOnOpen.gif');
            }
            return false;
        }

    });

    jQuery('#nav li').click(function() {
        if (jQuery(this).children('a').length == 0) {
            if (jQuery(this).children('ul').html() != null) {
                jQuery(this).parent('ul').children('li').children('ul').hide(slideSpeed);
                if (jQuery(this).children('ul').css('display') == "block") {
                    jQuery(this).children('ul').hide(slideSpeed);
                } else {
                    jQuery
                    (this).children('ul').show(slideSpeed);
                }
            }
        }
    });
    //End Required Section
    
    //Optional Section - Show Carrots
    var imgOffClosed = "url(images/imgOffClosed.gif) no-repeat 5px ";
    var imgOnClosed = "url(images/imgOnClosed.gif) no-repeat 5px ";
    var imgOnOpen = "url(images/imgOnOpen.gif) no-repeat 5px ";
    var charBeforeLB = 23; //characters before line break - you must calculate this - based on font-size and LI width;
    var paddingBig = 12; //push carrot arrow down (in pixels) when no there is a line break in the LI
    var paddingSmall = 8;  //push carrot arrow down (in pixels) when no there is no line break in the LI
    var maxLIHeight = 50; // max height of LI when list is closed

    jQuery('#nav > li').each(function() {

        var childText = jQuery(this).children('a').text();
        var topPadding = paddingBig;
        if (childText.length < charBeforeLB) { topPadding = paddingSmall; }
        if (jQuery(this).height() < maxLIHeight) {//list is closed

            if (jQuery(this).children('a').attr('class') == "Current") {
                jQuery(this).parent('li').children('img').attr('src', 'images/imgOnOpen.gif');
            } else {
                jQuery(this).parent('li').children('img').attr('src', 'images/imgOffClosed.gif');
            }

            
        }
        else {// list is open
            jQuery(this).children('img').attr('src', 'images/imgOnOpen.gif');
        }




    });
    
    //End Optional Section
});