<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'fullname_np','fullname_en', 'username', 'email', 'role', 'password', 'status', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function document() 
    {
        return $this->hasMany('App\Models\Document');
    }

    public function link() 
    {
        return $this->hasMany('App\Models\Link');
    }

    public function news() 
    {
        return $this->hasMany('App\Models\News');
    }

    public function notice() 
    {
        return $this->hasMany('App\Models\Notice');
    }

    public function staff() 
    {
        return $this->hasMany('App\Models\Staff');
    }
}
