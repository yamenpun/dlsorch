<?php
namespace App\HelperClass;


class AppHelper {

    /**
     * Generates html according to message_type and stores in
     * session flash storage
     *
     * @param $message_type bootstrap alert message type
     * @param $message html message
     */
    public function flash($message_type, $message)
    {
        $message_type = $this->checkBootstrapAlertClass($message_type);

        $message = "<div class=\"alert alert-" . $message_type . "\">
                        <button data-dismiss=\"alert\" class=\"close\" type=\"button\">
                            <i class=\"icon-remove\"></i>
                        </button>
                        " . $message . "
                        <br>
					</div>";

        request()->session()->flash('message', $message);
    }

    protected function checkBootstrapAlertClass($message_type)
    {
        $classes = ['info', 'success', 'warning', 'danger'];
        if (!in_array($message_type, $classes))
        {
            return 'info';
        }

        return $message_type;
    }

    public function getValidationErrorMsg($errors, $field_name)
    {
        if ($errors->has($field_name))
        {
            return '<strong class="help-block validation-error">' . $errors->first('caption_one') . "</strong>";
        }

        return '';
    }

    public function changeToKeyValArray($data, $key, $value)
    {
        $tmp = [];
        foreach ($data as $item)
        {
            $tmp[$item->$key] = $item->$value;
        }

        return $tmp;
    }

    public function getDataByLang($model, $column)
    {
        $lang = session()->get('app_language');
        $column_name = $column . '_' . $lang;

        return $model->$column_name;

    }

    public function validateLanguage($lang)
    {
        if ($lang == 'en')
        {
            return $lang;
        }
        if ($lang == 'np')
        {
            return $lang;
        }

        return 'np';
    }

    public function subMenu($data, $page_id)
    {
        $has_child = false;
        foreach ($data as $item)
        {
            if ($item->parent_id == $page_id)
            {
                $has_child = true;
            }
        }

        if ($has_child)
        {
            echo "<ul>";

            foreach ($data as $item)
            {
                if ($item->parent_id == $page_id)
                {
                    if ($item->page_type == 'content-page')
                    {
                        echo "<li><a href='" . route('page', ['url' => $item->url]) . "'>" . AppHelper::getDataByLang($item, 'title') . "</a>";
                    } else
                    {
                        echo "<li><a target='_blank' href='".$item->url."'>" . AppHelper::getDataByLang($item, 'title') . "</a>";
                    }
                    self::subMenu($data, $item->page_id);
                    echo "</li>";
                }
            }
            echo "</ul>";
        }


    }

}