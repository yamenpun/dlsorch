<?php

namespace App\Models;

class Page extends BaseModel
{
    protected $table = 'page';

    protected $fillable = ['id', 'title_np', 'title_en', 'content_np', 'content_en', 'url', 'image', 'page_type', 'order', 'status', 'created_at', 'updated_at'];
    
    public function menu()
    {
        return $this->belongsToMany('App\Models\Menu');
    }

}
