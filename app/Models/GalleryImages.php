<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryImages extends Model
{
    protected $table = 'gallery_images';

    protected $fillable = ['id', 'gallery_id', 'image', 'caption_np', 'caption_en','url', 'order', 'status', 'created_at', 'updated_at'];

    public function gallery()
    {
        return $this->belongsTo('App\Models\Gallery');
    }
}
