<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends BaseModel
{
    protected $table = 'news';

    protected $fillable = ['id', 'user_id', 'title_np', 'title_en', 'description_np', 'description_en', 'url', 'file', 'order', 'status', 'created_at', 'updated_at'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
