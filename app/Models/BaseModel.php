<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public function setStatusAttribute($value)
    {
        $value = (int) $value;
        $this->attributes['status'] = $value == 1?1:0;
    }


    public function scopeStatus($query, $status = 1)
    {
        return $query->where('status', $status);
    }
}
