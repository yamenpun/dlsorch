<?php

namespace App\Models;

class Document extends BaseModel
{
    protected $table = 'document';

    protected $fillable = ['id', 'user_id', 'title_np', 'title_en', 'description_np', 'description_en', 'url', 
            'document', 'caption_np','caption_en', 'order', 'status', 'created_at', 'updated_at'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
