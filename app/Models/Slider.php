<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends BaseModel
{
    protected $table = 'slider';

    protected $fillable = ['image', 'caption_np', 'caption_en', 'order', 'status', 'created_at', 'updated_at'];
    
}
