<?php

namespace App\Models;

class Contact extends BaseModel
{
    protected $table = 'contact';

    protected $fillable = ['id', 'address_np', 'address_en', 'phone_no', 'fax_no', 'post_box_no', 'email',
        'contact_person_name_np', 'contact_person_name_en','contact_person_post_np', 'contact_person_post_en', 'created_at', 'updated_at'];
    
}
