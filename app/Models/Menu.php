<?php

namespace App\Models;

class Menu extends BaseModel
{
    protected $table = 'menu';

    protected $fillable = ['id', 'title','position', 'order', 'status', 'created_at', 'updated_at'];

    public function page()
    {
        return $this->belongsToMany('App\Models\Page')->withPivot('page_id', 'order', 'parent_id');
    }
}
