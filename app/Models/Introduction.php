<?php

namespace App\Models;

class Introduction extends BaseModel
{
    protected $table = 'introduction';

    protected $fillable = ['id', 'title_np', 'title_en', 'description_np', 'description_en', 'created_at', 'updated_at'];
    
}
