<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends BaseModel
{
    protected $table = 'link';

    protected $fillable = ['user_id', 'title_np', 'title_en', 'url', 'order', 'status', 'created_at', 'updated_at'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
