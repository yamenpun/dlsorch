<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends BaseModel
{
    protected $table = 'staff';

    protected $fillable = ['user_id', 'fullname_np', 'fullname_en', 'designation_np', 'designation_en', 'division_np', 'division_en',
                                'email', 'phone_no', 'image', 'order', 'created_at', 'updated_at', 'status', 'created_at', 'updated_at'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
