<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notice extends BaseModel
{
    protected $table = 'notice';

    protected $fillable = ['id', 'user_id', 'title_np', 'title_en', 'description_np', 'description_en', 'url', 
            'image', 'caption_np','caption_en', 'order', 'status', 'created_at', 'updated_at'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
