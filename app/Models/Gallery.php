<?php

namespace App\Models;

class Gallery extends BaseModel
{
    protected $table = 'gallery';

    protected $fillable = ['id', 'title_np', 'title_en', 'description_np', 'description_en', 'feature_image','url', 'order', 'status', 'created_at', 'updated_at'];


    public function galleryImages()
    {
        return $this->hasMany('App\Models\GalleryImages');
    }

    

}


