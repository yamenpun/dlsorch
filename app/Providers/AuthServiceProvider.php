<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        User::class => UserPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        // User
        $gate->define('list-user', function ($user) {
            return $user->role === 'administrator';
        });
        $gate->define('view-user', function ($user) {
            return $user->role === 'administrator';
        });
        $gate->define('store-user', function ($user) {
            return $user->role === 'administrator';
        });
        $gate->define('update-user', function ($user) {
            return $user->role === 'administrator';
        });
        $gate->define('destroy-user', function ($user) {
            return $user->role === 'administrator';
        });
        $gate->define('profile-user', function ($user) {
            return true;
        });

    }
}
