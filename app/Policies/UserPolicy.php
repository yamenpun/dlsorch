<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function index(User $user, User $user_arg)
    {
        return $user->role !== 'administrator';
    }

    public function view(User $user)
    {
        return $user->role == 'administrator';
    }

    public function store(User $user)
    {
        return $user->role == 'administrator';
    }

    public function update(User $user)
    {
        return $user->role == 'administrator';
    }

    public function destroy(User $user)
    {
        return $user->role == 'administrator';
    }

}
