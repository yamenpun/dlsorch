<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class LoginValidation extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => 'required|email',
            'password' => 'required|max:32|min:3'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'You must insert email.',
            'email.email'    => 'Your email format is invalid.',
            'email.max'      => 'Your password must be minimum 3 character long.'
        ];
    }
}
