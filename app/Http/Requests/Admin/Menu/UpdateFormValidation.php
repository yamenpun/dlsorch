<?php

namespace App\Http\Requests\Admin\Menu;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'  => 'required|unique:menu,title,'.$this->request->get('id'),'.id',
            'order'  => 'numeric'
        ];
    }

    /**
     * Get the validation messages.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'title.required'    => 'Title field is required.',
            'title.unique'      => 'This title is already taken.',
            'order.numeric'     => 'Order MUST be a number.',
        ];
    }
}
