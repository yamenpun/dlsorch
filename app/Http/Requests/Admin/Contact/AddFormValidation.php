<?php

namespace App\Http\Requests\Admin\Contact;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address_np' => 'required',
            'address_en' => 'required',
            'phone_no'   => 'required',
            'email'      => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            'address_np.required'         => 'Nepali Address field is required.',
            'address_en.required'         => 'English Address field is required.',
            'phone_no.required'           => 'Phone Number field is required.',
            'email.required'              => 'Email field is required.',
            'email.email'                 => 'Email format is invalid.',
        ];
    }
}
