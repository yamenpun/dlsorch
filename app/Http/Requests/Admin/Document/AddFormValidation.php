<?php

namespace App\Http\Requests\Admin\Document;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'title_np'        		    => 'required',
                'title_en'        		    => 'required|unique:document,title_en',
                'file'              	    => 'required',
                'order'             	    => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'title_np.required' 				=> 'Nepali title field is Required.',
            'title_en.required' 				=> 'English title field is Required.',
            'title_en.unique' 					=> 'This english title is already taken.',
            'file.required'       				=> 'Document field is Required.',
            'order.required'       				=> 'Order field is required.',
            'order.numeric'       				=> 'Order Must be a number.',

        ];
    }

}
