<?php

namespace App\Http\Requests\Admin\News;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'title_np'        		=> 'required',
                'title_en'        		=> 'required|unique:notice,title_en',
                'file'                  => 'image',
                'order'             	=> 'numeric',
        ];
    }

    public function messages()
    {
        return [
            'title_np.required' 				=> 'Nepali title field is Required.',
            'title_en.required' 				=> 'English title field is Required.',
            'title_en.unique' 					=> 'This english title is already taken.',
            'file.image' 					    => 'Uploaded file MUST be image.',

        ];
    }

}
