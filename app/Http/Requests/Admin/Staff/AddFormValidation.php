<?php

namespace App\Http\Requests\Admin\Staff;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'fullname_np'       => 'required',
                'fullname_en'       => 'required',
                'designation_np'    => 'required',
                'designation_en'    => 'required',
                'division_np'       => 'required',
                'division_en'       => 'required',
                'file'              => 'image',
                'email'             => 'email',
                'phone_no'          => 'numeric|min:10',
                'order'             => 'numeric',
        ];
    }

    /**
     * Get the validation messages.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'fullname_np.required'    => 'Nepali Fullname field is Required.',
            'fullname_en.required'    => 'English Fullname field is Required.',
            'designation_np.required' => 'Nepali Designation field is Required.',
            'description_en.required' => 'English Designation field is Required.',
            'division_np.required'    => 'Nepali Division field is Required.',
            'division_en.required'    => 'English Division field is Required.',
            'file.image'              => 'Upload file Must be Image.',
            'phone_no.numeric'        => 'Mobile Number must be numeric.',
            'phone_no.min'            => 'Mobile Number must be at least 10 digits long.',

        ];
    }

}
