<?php

namespace App\Http\Requests\Admin\ContentPage;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_np' => 'required',
            'title_en' => 'required|unique:page,title_en,'.$this->request->get('id'),'.id',
            'order'    => 'numeric'
        ];
    }

    public function messages()
    {
        return [
            'title_np.required'                 => 'Nepali Title field is required.',
            'title_en.required'                 => 'English Title field is required.',
            'title_en.unique'                   => 'This english title is already taken.',
            'order.numeric'                     => 'Order Must be a number.',
        ];
    }
}
