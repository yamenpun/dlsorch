<?php

namespace App\Http\Requests\Admin\Introduction;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_np'       => 'required',
            'title_en'       => 'required',
            'description_np' => 'required',
            'description_en' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title_np.required'           => 'Nepali Title field is required.',
            'title_en.required'           => 'English Title field is required.',
            'description_np.required'     => 'Nepali Description field is required.',
            'description_en.required'     => 'English Description field is required.',
        ];
    }
}
