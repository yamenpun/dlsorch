<?php

namespace App\Http\Requests\Admin\GalleryImages;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'caption_np'        		=> 'required',
            'caption_en'                => 'required|unique:gallery_images,caption_en,'.$this->request->get('id'),'.id',
            'file'                      => 'image',
            'order'             	    => 'numeric',
        ];
    }

    public function messages()
    {
        return [
            'caption_np.required' 		=> 'Nepali caption field is Required.',
            'caption_en.required' 		=> 'English caption field is Required.',
            'caption_en.unique' 		=> 'This english caption is already taken.',
            'file.image'                => 'Uploaded file must be Image.',
            'order.numeric'       		=> 'Order Must be a number.',

        ];
    }
}
