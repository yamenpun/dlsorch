<?php

namespace App\Http\Requests\Admin\Slider;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'caption_np'        => 'required',
            'caption_en'        => 'required',
            'file'              => 'image',
            'order'             => 'numeric',
        ];
    }

    /**
     * Get the validation messages.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'caption_np.required'   => 'Nepali caption field is Required.',
            'caption_en.required'   => 'English caption field is Required.',
            'file.image'            => 'Uploaded slider file must be Image',

        ];
    }
}
