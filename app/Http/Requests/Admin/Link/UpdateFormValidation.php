<?php

namespace App\Http\Requests\Admin\Link;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_np'  => 'required',
            'title_en'  => 'required',
            'url'       => 'url|unique:link,url,'.$this->request->get('id'),'.id',
            'order'     => 'required|numeric',
        ];
    }

    /**
     * Get the validation messages.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'title_np.required' => 'Nepali Title field is required.',
            'title_en.required' => 'English Title field is required.',
        ];
    }
}
