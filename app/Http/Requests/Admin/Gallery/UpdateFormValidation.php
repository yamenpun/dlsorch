<?php

namespace App\Http\Requests\Admin\Gallery;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_np'        		    => 'required',
            'title_en'                  => 'required|unique:gallery,title_en,'.$this->request->get('id'),'.id',
            'file'                      => 'required|image',
            'order'             	    => 'numeric',
        ];
    }

    public function messages()
    {
        return [
            'title_np.required' 				=> 'Nepali title field is Required.',
            'title_en.required' 				=> 'English title field is Required.',
            'title_en.unique' 					=> 'This english title is already taken.',
            'file.required'                     => 'Image Field is required',
            'file.image'                        => 'Uploaded file MUST be Image',
            'order.numeric'       				=> 'Order Must be a number.',

        ];
    }
}
