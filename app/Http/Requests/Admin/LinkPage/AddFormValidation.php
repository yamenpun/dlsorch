<?php

namespace App\Http\Requests\Admin\LinkPage;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_np' => 'required',
            'title_en' => 'required',
            'url'      => 'required|url|unique:page,url',
            'order'    => 'numeric'
        ];
    }

    public function messages()
    {
        return [
            'title_np.required'                 => 'Nepali Title field is required.',
            'title_en.required'                 => 'English Title field is required.',
            'url.required'                      => 'URL field is required.',
            'url.unique'                        => 'This URL is already taken.',
            'order.numeric'                     => 'Order Must be a number.',
        ];
    }
}
