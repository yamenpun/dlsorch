<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'fullname_np'   => 'required',
            'fullname_en'   => 'required',
            'username'      => 'required|min:5|unique:users,username,'.$this->request->get('id').',id',
            'email'         => 'required|email|unique:users,email,'.$this->request->get('id').',id',
            'password'      => 'confirmed'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'fullname_np.required'    => 'Nepali fullname field is required.',
            'fullname_en.required'    => 'English fullname field is required.',
            'username.required'       => 'Username field is required.',
            'username.min'            => 'Username field Must be at least 5 character.',
            'username.unique'         => 'Username is already taken.',
            'email.required'          => 'Email field is required.',
            'email.email'             => 'Email format is invalid.',
            'email.unique'            => 'Email is already taken.',
            'password.confirmed'      => 'Password confirmation do not match.'
        ];
    }
}
