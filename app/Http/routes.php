<?php

// Route for authentication
Route::auth();

// Routes for the backend panel
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){

    Route::get('dashboard',                       ['as' => 'admin.dashboard',               'uses' => 'Admin\DashboardController@index']);

    Route::get('user',                            ['as' => 'admin.user',                    'uses' => 'Admin\UserController@index']);
    Route::get('user/add',                        ['as' => 'admin.user.add',                'uses' => 'Admin\UserController@create']);
    Route::post('user/store',                     ['as' => 'admin.user.store',              'uses' => 'Admin\UserController@store']);
    Route::get('user/view/{id}',                  ['as' => 'admin.user.view',               'uses' => 'Admin\UserController@view']);
    Route::get('user/edit/{id}',                  ['as' => 'admin.user.edit',               'uses' => 'Admin\UserController@edit']);
    Route::post('user/update/{id}',               ['as' => 'admin.user.update',             'uses' => 'Admin\UserController@update']);
    Route::get('user/delete/{id}',                ['as' => 'admin.user.delete',             'uses' => 'Admin\UserController@destroy']);
    Route::get('user/profile',                    ['as' => 'admin.user.profile',            'uses' => 'Admin\UserController@profile']);

    Route::get('staff',                           ['as' => 'admin.staff',                   'uses' => 'Admin\StaffController@index']);
    Route::get('staff/add',                       ['as' => 'admin.staff.add',               'uses' => 'Admin\StaffController@create']);
    Route::post('staff/store',                    ['as' => 'admin.staff.store',             'uses' => 'Admin\StaffController@store']);
    Route::get('staff/edit/{id}',                 ['as' => 'admin.staff.edit',              'uses' => 'Admin\StaffController@edit']);
    Route::post('staff/update/{id}',              ['as' => 'admin.staff.update',            'uses' => 'Admin\StaffController@update']);
    Route::get('staff/delete/{id}',               ['as' => 'admin.staff.delete',            'uses' => 'Admin\StaffController@destroy']);

    Route::get('menu',                            ['as' => 'admin.menu',                    'uses' => 'Admin\MenuController@index']);
    Route::get('menu/view/{id}',                  ['as' => 'admin.menu.view',               'uses' => 'Admin\MenuController@view']);
    Route::get('menu/add',                        ['as' => 'admin.menu.add',                'uses' => 'Admin\MenuController@create']);
    Route::post('menu/store',                     ['as' => 'admin.menu.store',              'uses' => 'Admin\MenuController@store']);
    Route::get('menu/edit/{id}',                  ['as' => 'admin.menu.edit',               'uses' => 'Admin\MenuController@edit']);
    Route::post('menu/update/{id}',               ['as' => 'admin.menu.update',             'uses' => 'Admin\MenuController@update']);
    Route::get('menu/delete/{id}',                ['as' => 'admin.menu.delete',             'uses' => 'Admin\MenuController@destroy']);
    Route::get('menu/page-html/{index}',          ['as' => 'admin.menu.renderPageHtml',     'uses' => 'Admin\MenuController@renderPageHtml']);

    Route::get('content-page',                    ['as' => 'admin.content-page',            'uses' => 'Admin\ContentPageController@index']);
    Route::get('content-page/add',                ['as' => 'admin.content-page.add',        'uses' => 'Admin\ContentPageController@create']);
    Route::post('content-page/store',             ['as' => 'admin.content-page.store',      'uses' => 'Admin\ContentPageController@store']);
    Route::get('content-page/edit/{id}',          ['as' => 'admin.content-page.edit',       'uses' => 'Admin\ContentPageController@edit']);
    Route::post('content-page/update/{id}',       ['as' => 'admin.content-page.update',     'uses' => 'Admin\ContentPageController@update']);
    Route::get('content-page/delete/{id}',        ['as' => 'admin.content-page.delete',     'uses' => 'Admin\ContentPageController@destroy']);

    Route::get('link-page',                       ['as' => 'admin.link-page',               'uses' => 'Admin\LinkPageController@index']);
    Route::get('link-page/add',                   ['as' => 'admin.link-page.add',           'uses' => 'Admin\LinkPageController@create']);
    Route::post('link-page/store',                ['as' => 'admin.link-page.store',         'uses' => 'Admin\LinkPageController@store']);
    Route::get('link-page/edit/{id}',             ['as' => 'admin.link-page.edit',          'uses' => 'Admin\LinkPageController@edit']);
    Route::post('link-page/update/{id}',          ['as' => 'admin.link-page.update',        'uses' => 'Admin\LinkPageController@update']);
    Route::get('link-page/delete/{id}',           ['as' => 'admin.link-page.delete',        'uses' => 'Admin\LinkPageController@destroy']);

    Route::get('slider',                          ['as' => 'admin.slider',                  'uses' => 'Admin\SliderController@index']);
    Route::get('slider/add',                      ['as' => 'admin.slider.add',              'uses' => 'Admin\SliderController@create']);
    Route::post('slider/store',                   ['as' => 'admin.slider.store',            'uses' => 'Admin\SliderController@store']);
    Route::get('slider/edit/{id}',                ['as' => 'admin.slider.edit',             'uses' => 'Admin\SliderController@edit']);
    Route::post('slider/update/{id}',             ['as' => 'admin.slider.update',           'uses' => 'Admin\SliderController@update']);
    Route::get('slider/delete/{id}',              ['as' => 'admin.slider.delete',           'uses' => 'Admin\SliderController@destroy']);
    Route::get('slider/default/{id}',             ['as' => 'admin.slider.default',          'uses' => 'Admin\SliderController@default']);

    Route::get('news',                            ['as' => 'admin.news',                    'uses' => 'Admin\NewsController@index']);
    Route::get('news/add',                        ['as' => 'admin.news.add',                'uses' => 'Admin\NewsController@create']);
    Route::post('news/store',                     ['as' => 'admin.news.store',              'uses' => 'Admin\NewsController@store']);
    Route::get('news/edit/{id}',                  ['as' => 'admin.news.edit',               'uses' => 'Admin\NewsController@edit']);
    Route::post('news/update/{id}',               ['as' => 'admin.news.update',             'uses' => 'Admin\NewsController@update']);
    Route::get('news/delete/{id}',                ['as' => 'admin.news.delete',             'uses' => 'Admin\NewsController@destroy']);

    Route::get('notice',                          ['as' => 'admin.notice',                  'uses' => 'Admin\NoticeController@index']);
    Route::get('notice/add',                      ['as' => 'admin.notice.add',              'uses' => 'Admin\NoticeController@create']);
    Route::post('notice/store',                   ['as' => 'admin.notice.store',            'uses' => 'Admin\NoticeController@store']);
    Route::get('notice/edit/{id}',                ['as' => 'admin.notice.edit',             'uses' => 'Admin\NoticeController@edit']);
    Route::post('notice/update/{id}',             ['as' => 'admin.notice.update',           'uses' => 'Admin\NoticeController@update']);
    Route::get('notice/delete/{id}',              ['as' => 'admin.notice.delete',           'uses' => 'Admin\NoticeController@destroy']);

    Route::get('useful-link',                     ['as' => 'admin.useful-link',              'uses' => 'Admin\LinkController@index']);
    Route::get('useful-link/add',                 ['as' => 'admin.useful-link.add',          'uses' => 'Admin\LinkController@create']);
    Route::post('useful-link/store',              ['as' => 'admin.useful-link.store',        'uses' => 'Admin\LinkController@store']);
    Route::get('useful-link/edit/{id}',           ['as' => 'admin.useful-link.edit',         'uses' => 'Admin\LinkController@edit']);
    Route::post('useful-link/update/{id}',        ['as' => 'admin.useful-link.update',       'uses' => 'Admin\LinkController@update']);
    Route::get('useful-link/delete/{id}',         ['as' => 'admin.useful-link.delete',       'uses' => 'Admin\LinkController@destroy']);

    Route::get('document',                        ['as' => 'admin.document',                'uses' => 'Admin\DocumentController@index']);
    Route::get('document/add',                    ['as' => 'admin.document.add',            'uses' => 'Admin\DocumentController@create']);
    Route::post('document/store',                 ['as' => 'admin.document.store',          'uses' => 'Admin\DocumentController@store']);
    Route::get('document/edit/{id}',              ['as' => 'admin.document.edit',           'uses' => 'Admin\DocumentController@edit']);
    Route::post('document/update/{id}',           ['as' => 'admin.document.update',         'uses' => 'Admin\DocumentController@update']);
    Route::get('document/delete/{id}',            ['as' => 'admin.document.delete',         'uses' => 'Admin\DocumentController@destroy']);

    Route::get('gallery',                         ['as' => 'admin.gallery',                 'uses' => 'Admin\GalleryController@index']);
    Route::get('gallery/add',                     ['as' => 'admin.gallery.add',             'uses' => 'Admin\GalleryController@create']);
    Route::post('gallery/store',                  ['as' => 'admin.gallery.store',           'uses' => 'Admin\GalleryController@store']);
    Route::get('gallery/edit/{id}',               ['as' => 'admin.gallery.edit',            'uses' => 'Admin\GalleryController@edit']);
    Route::post('gallery/update/{id}',            ['as' => 'admin.gallery.update',          'uses' => 'Admin\GalleryController@update']);
    Route::get('gallery/delete/{id}',             ['as' => 'admin.gallery.delete',          'uses' => 'Admin\GalleryController@destroy']);

    Route::get('images',                          ['as' => 'admin.images',                   'uses' => 'Admin\GalleryImagesController@index']);
    Route::get('images/add',                      ['as' => 'admin.images.add',               'uses' => 'Admin\GalleryImagesController@create']);
    Route::post('images/store',                   ['as' => 'admin.images.store',             'uses' => 'Admin\GalleryImagesController@store']);
    Route::get('images/edit/{id}',                ['as' => 'admin.images.edit',              'uses' => 'Admin\GalleryImagesController@edit']);
    Route::post('images/update/{id}',             ['as' => 'admin.images.update',            'uses' => 'Admin\GalleryImagesController@update']);
    Route::get('images/delete/{id}',              ['as' => 'admin.images.delete',            'uses' => 'Admin\GalleryImagesController@destroy']);

    Route::get('introduction',                    ['as' => 'admin.introduction',              'uses' => 'Admin\IntroductionController@index']);
    Route::get('introduction/add',                ['as' => 'admin.introduction.add',          'uses' => 'Admin\IntroductionController@create']);
    Route::post('introduction/store',             ['as' => 'admin.introduction.store',        'uses' => 'Admin\IntroductionController@store']);
    Route::get('introduction/edit/{id}',          ['as' => 'admin.introduction.edit',         'uses' => 'Admin\IntroductionController@edit']);
    Route::post('introduction/update/{id}',       ['as' => 'admin.introduction.update',       'uses' => 'Admin\IntroductionController@update']);
    Route::get('introduction/delete/{id}',        ['as' => 'admin.introduction.delete',       'uses' => 'Admin\IntroductionController@destroy']);

    Route::get('contact',                         ['as' => 'admin.contact',                   'uses' => 'Admin\ContactController@index']);
    Route::get('contact/add',                     ['as' => 'admin.contact.add',               'uses' => 'Admin\ContactController@create']);
    Route::post('contact/store',                  ['as' => 'admin.contact.store',             'uses' => 'Admin\ContactController@store']);
    Route::get('contact/edit/{id}',               ['as' => 'admin.contact.edit',              'uses' => 'Admin\ContactController@edit']);
    Route::post('contact/update/{id}',            ['as' => 'admin.contact.update',            'uses' => 'Admin\ContactController@update']);
    Route::get('contact/delete/{id}',             ['as' => 'admin.contact.delete',            'uses' => 'Admin\ContactController@destroy']);
});


Route::get('/',                                   ['as' => 'home',                          'uses' => 'Frontend\HomeController@index']);
Route::get('introduction',                        ['as' => 'introduction',                  'uses' => 'Frontend\IntroductionController@index']);
Route::get('news',                                ['as' => 'news',                          'uses' => 'Frontend\NewsController@index']);
Route::get('news/{id}',                           ['as' => 'news.view',                     'uses' => 'Frontend\NewsController@getNewsById']);
Route::get('notice',                              ['as' => 'notice',                        'uses' => 'Frontend\NoticeController@index']);
Route::get('notice/{id}',                         ['as' => 'notice.view',                   'uses' => 'Frontend\NoticeController@getNoticeById']);
Route::get('notice/docs/{id}',                    ['as' => 'notice.docs.view',              'uses' => 'Frontend\NoticeController@noticeDocsView']);
Route::get('gallery',                             ['as' => 'gallery',                       'uses' => 'Frontend\GalleryController@index']);
Route::get('gallery/{id}',                        ['as' => 'gallery.view',                  'uses' => 'Frontend\GalleryController@getImage']);
Route::get('image/{id}',                          ['as' => 'gallery.image',                 'uses' => 'Frontend\GalleryController@viewImage']);
Route::get('staff',                               ['as' => 'staff',                         'uses' => 'Frontend\StaffController@index']);
Route::get('feedback',                            ['as' => 'feedback',                      'uses' => 'Frontend\HomeController@feedback']);
Route::get('contact',                             ['as' => 'contact',                       'uses' => 'Frontend\ContactController@index']);
Route::get('document',                            ['as' => 'document',                      'uses' => 'Frontend\HomeController@document']);
Route::get('{url}',                               ['as' => 'page',                          'uses' => 'Frontend\PageController@index']);



