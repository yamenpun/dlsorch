<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use DB;
use Illuminate\Http\Request;

class HomeController extends FrontendBaseController {

    private $view_path = 'frontend.home';

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->trans_path = '';
    }

    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path . '.index'));
    }

    public function introduction()
    {
        return view(parent::loadDefaultVars($this->view_path . '.introduction'));
    }

    public function contact()
    {
        return view(parent::loadDefaultVars($this->view_path . '.contact'));
    }

    public function feedback()
    {
        return view(parent::loadDefaultVars($this->view_path . '.feedback'));
    }
}