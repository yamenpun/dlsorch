<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use DB;
use Illuminate\Http\Request;

class ContactController extends FrontendBaseController {

    private $view_path = 'frontend.contact';
    protected $model;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->trans_path = '';
    }


    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path . '.index'));
    }

}