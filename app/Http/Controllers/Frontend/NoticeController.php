<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use App\Models\Notice;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class NoticeController extends FrontendBaseController {

    private $view_path = 'frontend.notice';
    protected $model;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->trans_path = '';
    }


    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path . '.index'));
    }

    public function getNoticeById($id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;
       
        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function noticeDocsView()
    {
        $route_id = explode('/', URL::full());
        $id = array_pop($route_id);

        $data['docs'] = Notice::select('id','image', 'caption_np', 'caption_en')
            ->where('id', $id)
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.view_docs'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Notice::find($id);
        return $this->model;
    }

}