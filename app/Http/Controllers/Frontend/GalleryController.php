<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use DB;
use URL;
use App\Models\GalleryImages;
use Illuminate\Http\Request;

class GalleryController extends FrontendBaseController {

    private $view_path = 'frontend.gallery';
    protected $model;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->trans_path = '';
    }


    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function getImage()
    {
        $route_id = explode('/', URL::full());
        $data['id'] = array_pop($route_id);

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function viewImage()
    {
        $route_id = explode('/', URL::full());
        $id = array_pop($route_id);

        $data['image'] = GalleryImages::select('id','gallery_id', 'image', 'caption_np', 'caption_en', 'url', 'order')
            ->where('id', $id)
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.view_image'), compact('data'));
    }

}