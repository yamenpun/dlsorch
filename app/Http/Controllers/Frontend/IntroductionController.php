<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;

class IntroductionController extends FrontendBaseController {

    private $view_path = 'frontend.introduction';
    protected $model;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->trans_path = '';
    }


    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path . '.index'));
    }
}