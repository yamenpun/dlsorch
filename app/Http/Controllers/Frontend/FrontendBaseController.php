<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\AppBaseController;
use App\Models\Document;
use App\Models\GalleryImages;
use App\Models\Introduction;
use App\Models\Link;
use App\Models\Notice;
use App\Models\Slider;
use App\Models\Staff;
use App\Models\News;
use App\Models\Gallery;
use App\Models\Contact;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Route;
use View;
use DB;
use AppHelper;


class FrontendBaseController extends AppBaseController {

    protected $language;
    protected $introduction;
    protected $users;
    protected $image_url;
    protected $header_menus;
    protected $left_menus;
    protected $footer_menus;
    protected $links;
    protected $staffs;
    protected $news;
    protected $sliders;
    protected $notices;
    protected $notices_home;
    protected $documents;
    protected $galleries;
    protected $gallery_images;
    protected $contact;

    public function __construct(Request $request)
    {
        parent::__construct();

        if ($request->has('lan'))
        {
            $request->session()->put('app_language', AppHelper::validateLanguage($request->get('lan')));
        } else
        {
            if (!$request->session()->has('app_language'))
            {
                $request->session()->put('app_language', 'np');
            }
        }
        if (session()->get('app_language'))
        {
            App::setLocale(session()->get('app_language'));
        }
        $this->introduction = Introduction::select('id', 'title_np', 'title_en', 'description_np', 'description_en')->get();

        $this->users = User::select('id', 'fullname_np', 'fullname_en', 'username', 'email')->get();
        
        $this->header_menus = $this->headerMenuHierarchy();

        $this->left_menus = $this->leftMenuHierarchy();

        $this->links = Link::select('id', 'title_np', 'title_en', 'url')
            ->orderBy('order', 'DESC')
            ->status()
            ->get();
        $this->staffs = Staff::select('id', 'fullname_np', 'fullname_en', 'designation_np', 'designation_en', 'division_np', 'division_en', 'email', 'phone_no', 'image')
            ->orderBy('order')
            ->status()
            ->get();
        $this->news = News::select('id', 'title_np', 'title_en', 'description_np', 'description_en', 'file', 'url', 'created_at')
            ->orderBy('order', 'DESC')
            ->status()
            ->get();
        $this->sliders = Slider::select('id', 'image', 'caption_np', 'caption_en')
            ->orderBy('order', 'DESC')
            ->status()
            ->get();
        $this->notices = Notice::select('id', 'title_np', 'title_en', 'description_np', 'description_en', 'caption_np', 'caption_en', 'image', 'url', 'created_at')
            ->orderBy('order', 'DESC')
            ->status()
            ->get();
        $this->notices_home = Notice::select('id', 'title_np', 'title_en', 'description_np', 'description_en', 'caption_np', 'caption_en', 'image', 'url', 'created_at')
            ->orderBy('id', 'DESC')
            ->take(5)
            ->status()
            ->get();
        $this->documents = Document::select('id', 'title_np', 'title_en', 'description_np', 'description_en', 'url', 'document', 'caption_np', 'caption_en')
            ->orderBy('order', 'DESC')
            ->status()
            ->get();
        $this->galleries = Gallery::select('id', 'title_np', 'title_en', 'description_np', 'description_en', 'url', 'feature_image', 'created_at')
            ->orderBy('order', 'DESC')
            ->status()
            ->get();

        $this->gallery_images_home = GalleryImages::select('id','gallery_id', 'image', 'caption_np', 'caption_en', 'url')
            ->orderBy('order', 'DESC')
            ->get();
        $this->contact = Contact::select('id', 'address_np', 'address_en', 'phone_no', 'fax_no', 'post_box_no', 'email',
            'contact_person_name_np', 'contact_person_name_en','contact_person_post_np', 'contact_person_post_en', 'created_at', 'updated_at')->get();
    }

    protected function loadDefaultVars($view_path, $data = [])
    {
        View::composer($view_path, function ($view) use ($view_path, $data)
        {

            $view->with('view_path', parent::makeViewFolderPath($view_path));
            $view->with('image_path', config('broadway.url.frontend.image'));
            $view->with('composer_introduction', $this->introduction);
            $view->with('composer_users', $this->users);
            $view->with('composer_image_url', $this->image_url);
            $view->with('composer_header_menus', $this->header_menus);
            $view->with('composer_left_menus', $this->left_menus);
            $view->with('composer_links', $this->links);
            $view->with('composer_staffs', $this->staffs);
            $view->with('composer_news', $this->news);
            $view->with('composer_sliders', $this->sliders);
            $view->with('composer_notices', $this->notices);
            $view->with('composer_notices_home', $this->notices_home);
            $view->with('composer_documents', $this->documents);
            $view->with('composer_galleries', $this->galleries);
            $view->with('composer_gallery_images_home', $this->gallery_images_home);
            $view->with('composer_contact', $this->contact);
        });

        return $view_path;
    }

    public function headerMenuHierarchy()
    {

        $result = DB::table('menu')
            ->select('menu.position','menu_page.id','menu_page.page_id', 'menu_page.parent_id', 'menu_page.order', 'page.title_en', 'page.title_np', 'page.url', 'page.page_type')
            ->join('menu_page', 'menu.id', '=', 'menu_page.menu_id')
            ->join('page', 'menu_page.page_id', '=', 'page.id')
            ->where('menu.position', 'header_menu')
            ->get();

        return $result;

    }

    public function leftMenuHierarchy()
    {

        $result = DB::table('menu')
            ->select('menu.position','menu_page.id','menu_page.page_id', 'menu_page.parent_id', 'menu_page.order', 'page.title_en', 'page.title_np', 'page.url', 'page.page_type')
            ->join('menu_page', 'menu.id', '=', 'menu_page.menu_id')
            ->join('page', 'menu_page.page_id', '=', 'page.id')
            ->where('menu.position', 'left_menu')
            ->get();

        return $result;
    }
}