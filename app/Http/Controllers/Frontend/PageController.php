<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use App\Models\Page;
use URL;
use Illuminate\Http\Request;

class PageController extends FrontendBaseController {

    private $view_path = 'frontend.page';
    protected $model;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->trans_path = '';
    }


    public function index()
    {
        $route_id = explode('/', URL::full());
        $url = array_pop($route_id);

        $data['row'] = Page::select('id', 'title_np', 'title_en', 'content_np', 'content_en', 'image', 'created_at', 'updated_at')
            ->where('url', $url)
            ->status()
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }
}