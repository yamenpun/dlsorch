<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use App\Models\News;
use DB;
use Illuminate\Http\Request;

class NewsController extends FrontendBaseController {

    private $view_path = 'frontend.news';
    protected $model;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->trans_path = '';
    }


    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path . '.index'));
    }

    public function getNewsById($id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;
        
        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = News::find($id);
        return $this->model;
    }

}