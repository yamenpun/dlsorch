<?php

namespace App\Http\Controllers\Admin;

use App\Models\Menu;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\Admin\Menu\AddFormValidation;
use App\Http\Requests\Admin\Menu\UpdateFormValidation;
use DB;
use AppHelper;

class MenuController extends AdminBaseController
{
    protected $base_route = 'admin.menu';
    protected $view_path = 'admin.menu';
    protected $model;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['rows'] = Menu::select('id', 'title', 'position','order', 'status')
            ->orderBy('order', 'ASC')
            ->paginate($this->pagination_limit);
        return view(parent::loadDefaultVars($this->view_path.'.index'), compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [];
        $data['pages'] = Page::select('id', 'title_en', 'page_type')->orderBy('title_en', 'asc')->get();

        return view(parent::loadDefaultVars($this->view_path.'.create'), compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddFormValidation $request
     * @return $this
     */
    public function store(AddFormValidation $request)
    {
        //dd($request->all());
        $data = [];
        $data['row'] = Menu::create([

            'title'          => $request->get('title'),
            'position'       => $request->get('position'),
            'order'          => $request->get('order'),
            'status'         => $request->get('status'),
        ]);

        foreach ($request->pages as $key => $page) {
            $data['page_ids'][$page['page_id']]['order'] = $page['page_order'];
            $data['page_ids'][$page['page_id']]['parent_id'] = $page['parent_id'];
        }

        $data['row']->page()->sync($data['page_ids']);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;
        $data['pages'] = Page::select('id', 'title_en', 'page_type')->orderBy('title_en', 'asc')->get();
        $data['menu_page'] = $this->model->page;

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFormValidation $request
     * @param $id
     * @return $this
     */
    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }
        //dd($request->all());
        $data = [];
        $data['row'] = $this->model;
        $data['row']->update([

            'title'          => $request->get('title'),
            'position'       => $request->get('position'),
            'order'          => $request->get('order'),
            'status'         => $request->get('status'),

        ]);

        foreach ($request->pages as $key => $page) {
            $data['page_ids'][$page['page_id']]['order'] = $page['page_order'];
            $data['page_ids'][$page['page_id']]['parent_id'] = $page['parent_id'];
        }

        $data['row']->page()->sync($data['page_ids']);

        return redirect()->route($this->base_route)->withErrors(['message' => 'Record updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $this->model->page()->sync([]);
        Menu::destroy($id);

        return redirect()->route($this->base_route)->withErrors(['message' => 'Record deleted successfully.']);
    }

    public function renderPageHtml($index)
    {
        $data = [];
        $data['index'] = $index;
        $data['pages'] = Page::select('id', 'title_en')->orderBy('title_en', 'asc')->get();
        $data['message'] = 'Loading Page Html';
        $data['html'] = view(parent::loadDefaultVars($this->view_path.'._page_html'), compact('data'))->render();

        return response()->json(json_encode($data));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Menu::find($id);
        return $this->model;
    }
}
