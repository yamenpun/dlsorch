<?php

namespace App\Http\Controllers\Admin;

use App\Models\Link;
use App\Http\Requests;
use App\Http\Requests\Admin\Link\AddFormValidation;
use App\Http\Requests\Admin\Link\UpdateFormValidation;
use AppHelper;
use Auth;
use DB;

class LinkController extends AdminBaseController
{
    protected $base_route = 'admin.useful-link';
    protected $view_path = 'admin.useful-link';
    protected $model;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['rows'] = Link::select('id', 'title_np', 'title_en', 'url', 'order', 'status')
            ->orderBy('order', 'ASC')
            ->paginate($this->pagination_limit);
        return view(parent::loadDefaultVars($this->view_path.'.index'), compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path.'.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddFormValidation $request
     * @return $this
     */
    public function store(AddFormValidation $request)
    {
        $user = Auth::user();
        $data = [];
        $data['row'] = Link::create([
            'title_np'       => $request->get('title_np'),
            'title_en'       => $request->get('title_en'),
            'url'            => $request->get('url'),
            'order'          => $request->get('order'),
            'status'         => $request->get('status'),
            'user_id'        => $user->id,
        ]);
        
        AppHelper::flash('success', 'Record created Successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;
        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFormValidation $request
     * @param $id
     * @return $this
     */
    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();
        $data = $this->model;
        if ($request->get('url') === ''){
            $data->update([
                'title_np'       => $request->get('title_np'),
                'title_en'       => $request->get('title_en'),
                'order'          => $request->get('order'),
                'status'         => $request->get('status'),
                'user_id'        => $user->id,
            ]);
        }else
        {
            $data->update([
                'title_np'       => $request->get('title_np'),
                'title_en'       => $request->get('title_en'),
                'url'            => $request->get('url'),
                'order'          => $request->get('order'),
                'status'         => $request->get('status'),
                'user_id'        => $user->id,
            ]);
        }

        AppHelper::flash('success', 'Record updated successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        Link::destroy($id);

        AppHelper::flash('success', 'Record deleted successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Link::find($id);
        return $this->model;
    }
}
