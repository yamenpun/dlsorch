<?php
namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AppBaseController;
use Illuminate\Database\Eloquent\Collection;
use View;

class AdminBaseController extends AppBaseController
{
    protected $pagination_limit = 10;
    protected $banner_image_folder = 'banner';
    protected $user;

    public function __construct()
    {
        parent::__construct();
        $this->user = auth()->user();
    }

    protected function loadDefaultVars($view_path)
    {
        View::composer($view_path, function ($view) use ($view_path) {

            $view->with('view_path', parent::makeViewFolderPath($view_path));
            $view->with('base_route', $this->base_route);
            $view->with('trans_path', $this->makeViewFolderPath($view_path));
            $view->with('pagination_limit', $this->pagination_limit);
            $view->with('logged_in_user', $this->user);

        });

        return $view_path;
    }
    

    protected function changeToKeyValuePair(Collection $data)
    {
        $tmp = [];

       /* for ($i = 0; $i < $data->count(); $i++) {
            $tmp[$i]['id'] = $data[$i]->id;
            $tmp[$i]['text'] = $data[$i]->title;
        }*/
        foreach ($data as $key => $row) {
            $tmp[$key]['id'] = $row->id;
            $tmp[$key]['text'] = $row->title;
        }

       return $tmp;
    }


    protected function getArrayByKey(Collection $data, $key)
    {
        $tmp = [];
        foreach ($data as $item) {
            $tmp[] = $item->$key;
        }

        return $tmp;
    }


    protected function makeFolder($folder_name)
    {
        if (!file_exists(config('broadway.path.frontend.image').$folder_name)) {
            mkdir(config('broadway.path.frontend.image').$folder_name);
        }
    }


    protected function getArrayForDropdown($datas, $option_value, $option_text)
    {
        $tmp = [];
        foreach ($datas as $key => $data) {
            $tmp[$data->$option_value] = $data->$option_text;
        }

        return $tmp;
    }

}