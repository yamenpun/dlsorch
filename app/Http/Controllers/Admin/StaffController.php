<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Image;
use App\User;
use AppHelper;
use App\Models\Staff;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Requests\Admin\Staff\AddFormValidation;
use App\Http\Requests\Admin\Staff\UpdateFormValidation;

class StaffController extends AdminBaseController
{
    protected $base_route = 'admin.staff';
    protected $view_path = 'admin.staff';
    protected $model;
    protected $image_path;

    public function __construct()
    {
        parent::__construct();

        $this->image_path = config('broadway.path.frontend.image') . 'staff';
    }

    public function index()
    {
        $data = [];
        $data['rows'] = Staff::select('id', 'user_id', 'fullname_np', 'fullname_en', 'designation_np', 'designation_en', 'division_np', 'division_en','email', 'phone_no', 'image', 'order', 'created_at', 'updated_at', 'status')
            ->orderBy('order', 'ASC')
            ->paginate($this->pagination_limit);
        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function create(Request $request)
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddFormValidation $request
     * @return $this
     */
    public function store(AddFormValidation $request)
    {
        $user = Auth::user();
        $data = [];
        $data['row'] = Staff::create([
           
            'fullname_np'       => $request->get('fullname_np'),
            'fullname_en'       => $request->get('fullname_en'),
            'designation_np'    => $request->get('designation_np'),
            'designation_en'    => $request->get('designation_en'),
            'division_np'       => $request->get('division_np'),
            'division_en'       => $request->get('division_en'),
            'email'             => $request->get('email'),
            'phone_no'          => $request->get('phone_no'),
            'order'             => $request->get('order'),
            'status'            => $request->get('status'),
            'user_id'           => $user->id,
        ]);

        if (!file_exists($this->image_path)) {
            mkdir($this->image_path);
        }

        if ($file = $request->file('file')) {
            $file_name = rand(1857, 9899) . '_' . $file->getClientOriginalName();

            $file->move($this->image_path, $file_name);

            $data['row']->image = $file_name;
            $data['row']->save();
        }

        AppHelper::flash('success', 'Record created Successfully.');
        return redirect()->route($this->base_route);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;
        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFormValidation $request
     * @param $id
     * @return $this
     */
    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();
        $data = $this->model;
        $data->update([
            'fullname_np'       => $request->get('fullname_np'),
            'fullname_en'       => $request->get('fullname_en'),
            'designation_np'    => $request->get('designation_np'),
            'designation_en'    => $request->get('designation_en'),
            'division_np'       => $request->get('division_np'),
            'division_en'       => $request->get('division_en'),
            'email'             => $request->get('email'),
            'phone_no'          => $request->get('phone_no'),
            'order'             => $request->get('order'),
            'status'            => $request->get('status'),
            'user_id'           => $user->id,
        ]);

        // Image
        if ($file = $request->file('file')) {

            $file_name = time() . '_' . $file->getClientOriginalName();
            $file->move($this->image_path, $file_name);

            // remove old image
            if ($data->image !== '' && $data->image !== null) {

                $old_img = $this->image_path . '\\' . $data->image;
                @unlink($old_img);

            }

            $data->image = $file_name;
            $data->save();

        }

        AppHelper::flash('success', 'Record updated successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$this->idExist($id)) {
            AppHelper::flash('warning', 'Invalid Request.');
            return redirect()->route($this->base_route);
        }

        $result = Staff::destroy($id);
        if ($result) {
            if ($this->model->image !== '' && $this->model->image !== null) {
                $file_path = config('broadway.path.frontend.image') . 'staff\\' . $this->model->image;
                if (file_exists($file_path)){
                    unlink($file_path);
                }
            }
        }

        AppHelper::flash('success', 'Record deleted successfully.');
        return redirect()->route($this->base_route);
    }


    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Staff::find($id);
        return $this->model;
    }

}
