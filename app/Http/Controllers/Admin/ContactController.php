<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Gate;
use App\Models\Contact;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Admin\Contact\AddFormValidation;
use App\Http\Requests\Admin\Contact\UpdateFormValidation;


class ContactController extends AdminBaseController {

    protected $view_path  = 'admin.contact';
    protected $base_route = 'admin.contact';
    protected $model;

    public function index()
    {
        $data = [];
        $data['rows'] = Contact::select('id', 'address_np', 'address_en', 'phone_no', 'fax_no', 'post_box_no', 'email',
            'contact_person_name_np', 'contact_person_name_en','contact_person_post_np', 'contact_person_post_en')->paginate();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        $data = [];
        $data['rows'] = Contact::find($id);

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        Contact::create([
            'address_en'             => $request->get('address_en'),
            'address_np'             => $request->get('address_np'),
            'phone_no'               => $request->get('phone_no'),
            'fax_no'                 => $request->get('fax_no'),
            'post_box_no'            => $request->get('post_box_no'),
            'email'                  => $request->get('email'),
            'contact_person_name_np' => $request->get('contact_person_name_np'),
            'contact_person_name_en' => $request->get('contact_person_name_en'),
            'contact_person_post_np' => $request->get('contact_person_post_np'),
            'contact_person_post_en' => $request->get('contact_person_post_en'),
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route);
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }
        $data = $this->model;
        $data->update([
            'address_en'             => $request->get('address_en'),
            'address_np'             => $request->get('address_np'),
            'phone_no'               => $request->get('phone_no'),
            'fax_no'                 => $request->get('fax_no'),
            'post_box_no'            => $request->get('post_box_no'),
            'email'                  => $request->get('email'),
            'contact_person_name_np' => $request->get('contact_person_name_np'),
            'contact_person_name_en' => $request->get('contact_person_name_en'),
            'contact_person_post_np' => $request->get('contact_person_post_np'),
            'contact_person_post_en' => $request->get('contact_person_post_en'),
        ]);

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route);
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route);
        }
        Contact::destroy($id);
        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route);
    }


    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Contact::find($id);

        return $this->model;
    }
}
