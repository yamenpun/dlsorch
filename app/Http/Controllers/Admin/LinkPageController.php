<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\LinkPage\AddFormValidation;
use App\Http\Requests\Admin\LinkPage\UpdateFormValidation;
use App\Models\Page;
use DB;
use Auth;
use Image;
use AppHelper;



class LinkPageController extends AdminBaseController
{
    protected $base_route = 'admin.link-page';
    protected $view_path = 'admin.link-page';
    protected $page_type;
    protected $model;

    public function __construct()
    {
        parent::__construct();

        $this->page_type = 'link-page';
    }

    public function index()
    {
        $data = [];
        $data['rows'] = Page::select('id','title_np', 'title_en', 'url', 'order', 'status')
            ->where('page_type', 'link-page')
            ->orderBy('order', 'ASC')
            ->paginate($this->pagination_limit);

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddFormValidation $request
     * @return $this
     */
    public function store(AddFormValidation $request)
    {
        $data = [];
        $data['row'] = Page::create([
            'title_np'          => $request->get('title_np'),
            'title_en'          => $request->get('title_en'),
            'url'               => $request->get('url'),
            'page_type'         => $this->page_type,
            'order'             => $request->get('order'),
            'status'            => $request->get('status'),
        ]);

        AppHelper::flash('success', 'Record created Successfully.');
        return redirect()->route($this->base_route);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;
        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFormValidation $request
     * @param $id
     * @return $this
     */
    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = $this->model;
        if ($request->get('url') === ''){
            $data->update([
                'title_np'          => $request->get('title_np'),
                'title_en'          => $request->get('title_en'),
                'page_type'         => $this->page_type,
                'order'             => $request->get('order'),
                'status'            => $request->get('status'),
            ]);
        }else
        {
            $data->update([
                'title_np'          => $request->get('title_np'),
                'title_en'          => $request->get('title_en'),
                'url'               => $request->get('url'),
                'page_type'         => $this->page_type,
                'order'             => $request->get('order'),
                'status'            => $request->get('status'),
            ]);
        }
        
        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$this->idExist($id)) {
            AppHelper::flash('warning', 'Invalid Request.');
            return redirect()->route($this->base_route);
        }

        Page::destroy($id);

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route);
    }


    /**
     * Helper Methods
     */

    /**
     * @param $id
     * @return mixed
     */
    protected function idExist($id)
    {
        $this->model = Page::find($id);
        return $this->model;
    }

}
