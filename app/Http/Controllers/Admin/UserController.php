<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Gate;
use App\User;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Admin\User\AddFormValidation;
use App\Http\Requests\Admin\User\UpdateFormValidation;


class UserController extends AdminBaseController {

    protected $view_path = 'admin.user';
    protected $base_route = 'admin.user';
    protected $model;

    public function index()
    {
        if (Gate::denies('list-user'))
        {
            abort(401);
        }

        $data = [];
        $data['rows'] = User::select('id', 'fullname_en', 'fullname_np', 'username', 'email', 'role', 'created_at', 'updated_at', 'status')
            ->paginate($this->pagination_limit);

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        if (Gate::denies('view-user'))
        {
            abort(401);
        }
        $data = [];
        $data['rows'] = User::find($id);

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function create()
    {
        if (Gate::denies('store-user'))
        {
            abort(401);
        }

        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        if (Gate::denies('store-user'))
        {
            abort(401);
        }
        User::create([
            'fullname_en' => $request->get('fullname_en'),
            'fullname_np' => $request->get('fullname_np'),
            'username'    => $request->get('username'),
            'email'       => $request->get('email'),
            'password'    => bcrypt($request->get('password')),
            'role'        => $request->get('role'),
            'status'      => $request->get('status')
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route);
    }

    public function edit($id)
    {
        if (Gate::denies('update-user'))
        {
            abort(401);
        }
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }
        //$this->model->role = 'Role Id';

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (Gate::denies('update-user'))
        {
            abort(401);
        }
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }
        $data = $this->model;

        if ($request->get('password') === '')
        {
            $data->update([
                'fullname_en' => $request->get('fullname_en'),
                'fullname_np' => $request->get('fullname_np'),
                'username'    => $request->get('username'),
                'email'       => $request->get('email'),
                'password'    => $data->password,
                'role'        => $request->get('role'),
                'status'      => $request->get('status')
            ]);
        } else
        {
            $data->update([
                'fullname_en' => $request->get('fullname_en'),
                'fullname_np' => $request->get('fullname_np'),
                'username'    => $request->get('username'),
                'email'       => $request->get('email'),
                'password'    => bcrypt($request->get('password')),
                'role'        => $request->get('role'),
                'status'      => $request->get('status')
            ]);
        }

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route);
    }

    public function destroy($id)
    {
        if (Gate::denies('destroy-user'))
        {
            abort(401);
        }
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route);
        }
        User::destroy($id);
        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route);
    }

    public function profile()
    {
        if (Gate::denies('profile-user'))
        {
            abort(401);
        }
        $data = [];
        $data['rows'] = Auth::user();

        return view(parent::loadDefaultVars($this->view_path . '.profile'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = User::find($id);

        return $this->model;
    }
}
