<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\slider\AddFormValidation;
use App\Http\Requests\Admin\slider\UpdateFormValidation;
use App\Http\Requests;
use AppHelper;
use Image;

class SliderController extends AdminBaseController
{
    protected $base_route = 'admin.slider';
    protected $view_path = 'admin.slider';
    protected $model;
    protected $image_path;

    public function __construct()
    {
        parent::__construct();

        $this->image_path = config('broadway.path.frontend.image') . 'slider';
        
    }

    public function index()
    {
        $data = [];
        $data['rows'] = Slider::select('id', 'image', 'caption_np', 'caption_en','order', 'status', 'created_at', 'updated_at')
            ->orderBy('order', 'ASC')
            ->paginate($this->pagination_limit);
        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function create(Request $request)
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddFormValidation $request
     * @return $this
     */
    public function store(AddFormValidation $request)
    {
        $data = [];
        $data['row'] = Slider::create([
            'caption_np'        => $request->get('caption_np'),
            'caption_en'        => $request->get('caption_en'),
            'order'             => $request->get('order'),
            'status'            => $request->get('status'),
        ]);

        if (!file_exists($this->image_path)) {
            mkdir($this->image_path);
        }

        if ($file = $request->file('file')) {
            $file_name = rand(1857, 9899) . '_' . $file->getClientOriginalName();

            $file->move($this->image_path, $file_name);

            $data['row']->image = $file_name;
            $data['row']->save();

        }

        AppHelper::flash('success', 'Data created Successfully.');
        return redirect()->route($this->base_route);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;
        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFormValidation $request
     * @param $id
     * @return $this
     */
    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = $this->model;
        $data->update([
            'caption_np'        => $request->get('caption_np'),
            'caption_en'        => $request->get('caption_en'),
            'order'             => $request->get('order'),
            'status'            => $request->get('status'),
        ]);

        // Image
        if ($file = $request->file('file')) {
            $file_path = $this->image_path . '\\' . $data->image;
            if (file_exists($file_path))
                unlink($file_path);

            $file_name = rand(1857, 9899) . '_' . $file->getClientOriginalName();
            $file->move($this->image_path, $file_name);

            // remove old image
            if ($data->image !== '' && $data->image !== null) {
                $old_img = $this->image_path . '\\' . $data->image;
                @unlink($old_img);

            }

            $data->image = $file_name;
            $data->save();

        }

        AppHelper::flash('success', 'Record updated successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$this->idExist($id)) {
            AppHelper::flash('warning', 'Invalid Request.');
            return redirect()->route($this->base_route);
        }

        $result = Slider::destroy($id);
        if ($result) {
            if ($this->model->image !== '' && $this->model->image !== null) {
                $file_path = config('broadway.path.frontend.image') . 'slider\\' . $this->model->image;
                if (file_exists($file_path)){
                    unlink($file_path);
                }
            }
        }

        AppHelper::flash('success', 'Record deleted successfully.');
        return redirect()->route($this->base_route);
    }


    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = slider::find($id);
        return $this->model;
    }

}

