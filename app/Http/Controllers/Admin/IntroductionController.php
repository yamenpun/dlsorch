<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Gate;
use App\Models\Introduction;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Admin\Introduction\AddFormValidation;
use App\Http\Requests\Admin\Introduction\UpdateFormValidation;


class IntroductionController extends AdminBaseController {

    protected $view_path = 'admin.introduction';
    protected $base_route = 'admin.introduction';
    protected $model;

    public function index()
    {
        $data = [];
        $data['rows'] = Introduction::select('id', 'title_en', 'title_np', 'description_np', 'description_en', 'created_at', 'updated_at')
            ->paginate($this->pagination_limit);

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        $data = [];
        $data['rows'] = Introduction::find($id);

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        Introduction::create([
            'title_en'       => $request->get('title_en'),
            'title_np'       => $request->get('title_np'),
            'description_en' => $request->get('description_en'),
            'description_np' => $request->get('description_np'),
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route);
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }
        $data = $this->model;

        $data->update([
            'title_en'       => $request->get('title_en'),
            'title_np'       => $request->get('title_np'),
            'description_en' => $request->get('description_en'),
            'description_np' => $request->get('description_np'),
        ]);


        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route);
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route);
        }
        Introduction::destroy($id);
        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route);
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Introduction::find($id);

        return $this->model;
    }
}
