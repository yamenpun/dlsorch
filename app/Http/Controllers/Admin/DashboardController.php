<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends AdminBaseController
{
	protected $view_path = 'admin.dashboard';
    protected $base_route = 'admin.dashboard';

    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path . '.index'));
    }
}
