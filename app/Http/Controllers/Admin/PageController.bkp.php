<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Page\AddFormValidation;
use App\Http\Requests\Admin\Page\UpdateFormValidation;
use App\Models\Page;
use DB;
use Auth;
use Image;
use AppHelper;



class PageController extends AdminBaseController
{
    protected $base_route = 'admin.link-page';
    protected $view_path = 'admin.link-page';
    protected $model;
    protected $image_path;

    public function __construct()
    {
        parent::__construct();

        $this->image_path = config('broadway.path.frontend.image') . 'page';
    }

    public function index(Request $request)
    {
        $data = [];
        $data['rows'] = Page::select('id','title_np', 'title_en', 'content_np', 'content_en', 'url', 'image', 'order', 'status')
            ->orderBy('order', 'ASC')
            ->paginate($this->pagination_limit);

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddFormValidation $request
     * @return $this
     */
    public function store(AddFormValidation $request)
    {
        $data = [];
        $data['row'] = Page::create([
            'title_np'          => $request->get('title_np'),
            'title_en'          => $request->get('title_en'),
            'content_np'        => $request->get('content_np'),
            'content_en'        => $request->get('content_en'),
            'url'               => str_slug($request->get('title_en')),
            'order'             => $request->get('order'),
            'status'            => $request->get('status'),
        ]);

        if (!file_exists($this->image_path)) {
            mkdir($this->image_path);
        }

        if ($file = $request->file('file')) {
            $file_name = time() . '_' . $file->getClientOriginalName();

            $file->move($this->image_path, $file_name);

            $data['row']->image = $file_name;
            $data['row']->save();
        }

        AppHelper::flash('success', 'Record created Successfully.');
        return redirect()->route($this->base_route);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;
        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFormValidation $request
     * @param $id
     * @return $this
     */
    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = $this->model;
        $data->update([
            'title_np'          => $request->get('title_np'),
            'title_en'          => $request->get('title_en'),
            'content_np'        => $request->get('content_np'),
            'content_en'        => $request->get('content_en'),
            'url'               => str_slug($request->get('title_en')),
            'order'             => $request->get('order'),
            'status'            => $request->get('status'),
        ]);

        // Image
        if ($file = $request->file('file')) {

            $file_name = time() . '_' . $file->getClientOriginalName();
            $file->move($this->image_path, $file_name);

            // remove old image
            if ($data->image !== '' && $data->image !== null) {

                $old_img = $this->image_path . '\\' . $data->image;
                @unlink($old_img);

            }


            $data->image = $file_name;
            $data->save();

        }

        AppHelper::flash('success', 'Record updated successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$this->idExist($id)) {
            AppHelper::flash('warning', 'Invalid Request.');
            return redirect()->route($this->base_route);
        }

        $result = Page::destroy($id);
        if ($result) {
            if ($this->model->image !== '' && $this->model->image !== null) {
                $file_path = config('broadway.path.frontend.image') . 'page\\' . $this->model->image;
                if (file_exists($file_path)){
                    unlink($file_path);
                }
            }
        }

        AppHelper::flash('success', 'Record deleted successfully.');
        return redirect()->route($this->base_route);
    }


    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Page::find($id);
        return $this->model;
    }

}
