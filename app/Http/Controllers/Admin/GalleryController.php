<?php

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use App\Http\Requests;
use App\Http\Requests\Admin\Gallery\AddFormValidation;
use App\Http\Requests\Admin\Gallery\UpdateFormValidation;
use DB;
use AppHelper;

class GalleryController extends AdminBaseController
{
    protected $base_route = 'admin.gallery';
    protected $view_path = 'admin.gallery';
    protected $model;
    protected $image_path;

    public function __construct()
    {
        parent::__construct();

        $this->image_path = config('broadway.path.frontend.image') . 'gallery';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['rows'] = Gallery::select('id', 'title_np', 'title_en','description_np', 'description_en','feature_image', 'url', 'order', 'status')
            ->orderBy('order', 'ASC')
            ->paginate($this->pagination_limit);
        return view(parent::loadDefaultVars($this->view_path.'.index'), compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path.'.create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddFormValidation $request
     * @return $this
     */
    public function store(AddFormValidation $request)
    {
        $data = [];
        $data['row'] = Gallery::create([
            'title_np'       => $request->get('title_np'),
            'title_en'       => $request->get('title_en'),
            'description_np' => $request->get('description_np'),
            'description_en' => $request->get('description_en'),
            'url'            => str_slug($request->get('title_en')),
            'order'          => $request->get('order'),
            'status'         => $request->get('status'),
        ]);

        if (!file_exists($this->image_path)) {
            mkdir($this->image_path);
        }

        if ($file = $request->file('file')) {
            $file_name = 'feature_'.rand(1857, 9899) . '_' . $file->getClientOriginalName();

            $file->move($this->image_path, $file_name);

            $data['row']->feature_image = $file_name;
            $data['row']->save();

        }

        AppHelper::flash('success', 'Data created Successfully.');
        return redirect()->route($this->base_route);

        AppHelper::flash('success', 'Record created Successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;
        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFormValidation $request
     * @param $id
     * @return $this
     */
    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = $this->model;
        $data->update([

            'title_np'       => $request->get('title_np'),
            'title_en'       => $request->get('title_en'),
            'description_np' => $request->get('description_np'),
            'description_en' => $request->get('description_en'),
            'url'            => str_slug($request->get('title_en')),
            'order'          => $request->get('order'),
            'status'         => $request->get('status'),

        ]);

        // Image
        if ($file = $request->file('file')) {
            $file_path = $this->image_path . '\\' . $data->feature_image;

            if (file_exists($file_path)){
                unlink($file_path);
            }

            $file_name = 'feature_'.rand(1857, 9899) . '_' . $file->getClientOriginalName();
            $file->move($this->image_path, $file_name);

            // remove old image
            if ($data->feature_image !== '' && $data->feature_image !== null) {

                $old_img = $this->image_path . '\\'  . $data->feature_image;
                @unlink($old_img);

            }


            $data->feature_image = $file_name;
            $data->save();

        }

        AppHelper::flash('success', 'Record updated Successfully.');
        return redirect()->route($this->base_route);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $result = Gallery::destroy($id);
        if ($result) {
            if ($this->model->feature_image !== '' && $this->model->feature_image !== null) {
                $file_path = config('broadway.path.frontend.image') . 'gallery\\' . $this->model->feature_image;
                if (file_exists($file_path)){
                    unlink($file_path);
                }
            }
        }

        return redirect()->route($this->base_route)->withErrors(['message' => 'Record deleted successfully.']);
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Gallery::find($id);
        return $this->model;
    }
}
