<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\GalleryImages\AddFormValidation;
use App\Http\Requests\Admin\GalleryImages\UpdateFormValidation;
use App\Models\GalleryImages;
use App\Models\Gallery;
use DB;
use Image;
use AppHelper;



class GalleryImagesController extends AdminBaseController
{
    protected $base_route = 'admin.images';
    protected $view_path = 'admin.images';
    protected $model;
    protected $image_path;

    public function __construct()
    {
        parent::__construct();

        $this->image_path = config('broadway.path.frontend.image') . 'gallery';
    }

    public function index()
    {
        $data = [];
        $data['galleries'] = Gallery::select('id', 'title_np', 'title_en')->orderBy('order', 'ASC')
            ->get();
        $data['rows'] = GalleryImages::select('id', 'gallery_id', 'image', 'caption_np', 'caption_en', 'order','url', 'status')
            ->orderBy('order', 'ASC')
            ->paginate($this->pagination_limit);

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function create(Request $request)
    {
        $data = [];
        $data['galleries'] = Gallery::select('id', 'title_en')->get();
        return view(parent::loadDefaultVars($this->view_path . '.create'), compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddFormValidation $request
     * @return $this
     */
    public function store(AddFormValidation $request)
    {
        //dd($request->all());
        $data = [];
        $data['row'] = GalleryImages::create([
            'gallery_id'        => $request->get('gallery_id'),
            'caption_np'        => $request->get('caption_np'),
            'caption_en'        => $request->get('caption_en'),
            'url'               => str_slug($request->get('caption_en')),
            'order'             => $request->get('order'),
            'status'            => $request->get('status'),
        ]);

        if (!file_exists($this->image_path)) {
            mkdir($this->image_path);
        }

        if ($file = $request->file('file')) {
            $file_name = rand(1857, 9899) . '_' . $file->getClientOriginalName();

            $file->move($this->image_path, $file_name);

            $data['row']->image = $file_name;
            $data['row']->save();


            // Resize
            $config = config('broadway.image-dimensions.gallery');
            foreach ($config as $dimension) {

                // create instance
                $img = Image::make($this->image_path . '\\' . $file_name);
                // resize image to fixed size
                $img->resize($dimension['width'], $dimension['height']);
                $img->save($this->image_path . '\\' . $dimension['width'] . '_' . $dimension['height'] . '_' . $file_name);

            }

        }

        AppHelper::flash('success', 'Record created Successfully.');
        return redirect()->route($this->base_route);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['galleries'] = Gallery::select('id', 'title_en')->get();
        $data['row'] = $this->model;
        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFormValidation $request
     * @param $id
     * @return $this
     */
    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id)) {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = $this->model;
        $data->update([
            'gallery_id'        => $request->get('gallery_id'),
            'caption_np'        => $request->get('caption_np'),
            'caption_en'        => $request->get('caption_en'),
            'url'               => str_slug($request->get('caption_en')),
            'order'             => $request->get('order'),
            'status'            => $request->get('status'),
        ]);

        // Image
        if ($file = $request->file('file')) {
            $file_path = $this->image_path . '\\' . $data->image;
            if (file_exists($file_path))
                unlink($file_path);

            $file_name = rand(1857, 9899) . '_' . $file->getClientOriginalName();
            $file->move($this->image_path, $file_name);

            // remove old image
            if ($data->image !== '' && $data->image !== null) {

                $config = config('broadway.image-dimensions.gallery');
                foreach ($config as $dimension) {
                    $old_img = $this->image_path . '\\' . $dimension['width'] . '_' . $dimension['height'] . '_' . $data->image;

                    @unlink($old_img);

                    $img = Image::make($this->image_path . '\\' . $file_name);
                    $img->resize($dimension['width'], $dimension['height']);
                    $img->save($this->image_path . '\\' . $dimension['width'] . '_' . $dimension['height'] . '_' . $file_name);
                }

            }


            $data->image = $file_name;
            $data->save();

        }

        AppHelper::flash('success', 'Record updated successfully.');
        return redirect()->route($this->base_route);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$this->idExist($id)) {
            AppHelper::flash('warning', 'Invalid Request.');
            return redirect()->route($this->base_route);
        }

        $result = GalleryImages::destroy($id);
        if ($result) {
            if ($this->model->image !== '' && $this->model->image !== null) {
                $file_path = config('broadway.path.frontend.image') . 'gallery\\' . $this->model->image;
                if (file_exists($file_path))
                    unlink($file_path);

                $config = config('broadway.image-dimensions.gallery');
                foreach ($config as $dimension) {
                    if (file_exists($this->image_path . '\\' . $dimension['width'] . '_' . $dimension['height'] . '_' . $this->model->image))
                        unlink($this->image_path . '\\' . $dimension['width'] . '_' . $dimension['height'] . '_' . $this->model->image);
                }

            }
        }

        AppHelper::flash('success', 'Record deleted successfully.');
        return redirect()->route($this->base_route);
    }


    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = GalleryImages::find($id);
        return $this->model;
    }

}
