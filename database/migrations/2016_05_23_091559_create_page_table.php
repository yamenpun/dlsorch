
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_np', 255);
            $table->string('title_en', 255)->unique();
            $table->longText('content_np')->nullable();
            $table->longText('content_en')->nullable();
            $table->integer('parent')->default(0);
            $table->string('url', 255)->index();
            $table->text('image')->nullable();
            $table->unsignedInteger('order')->index();
            $table->boolean('status')->default(0)->index();
            $table->enum('page_type', ['content-page', 'link-page'])->default('content-page');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page');
    }
}
