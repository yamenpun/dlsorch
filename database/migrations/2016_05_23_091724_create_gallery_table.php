<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_np', 255);
            $table->string('title_en', 255)->unique();
            $table->text('description_np')->nullable();
            $table->text('description_en')->nullable();
            $table->text('feature_image');
            $table->string('url', 100)->index();
            $table->unsignedInteger('order')->index();
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery');
    }
}
