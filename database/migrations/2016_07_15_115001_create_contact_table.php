<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address_np');
            $table->string('address_en');
            $table->string('phone_no');
            $table->string('fax_no')->nullable();
            $table->string('post_box_no')->nullable();
            $table->string('email');
            $table->string('contact_person_name_np')->nullable();
            $table->string('contact_person_name_en')->nullable();
            $table->string('contact_person_post_np')->nullable();
            $table->string('contact_person_post_en')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contact');
    }
}
