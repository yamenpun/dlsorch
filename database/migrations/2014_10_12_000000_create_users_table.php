<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname_np', 50);
            $table->string('fullname_en', 50);
            $table->string('username', 30)->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('role', ['administrator', 'normal'])->index();
            $table->rememberToken();
            $table->boolean('status')->default(1);
            $table->timestamps();     
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
