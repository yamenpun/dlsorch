<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('title_np', 255);
            $table->string('title_en', 255)->unique();
            $table->text('description_np')->nullable();
            $table->text('description_en')->nullable();
            $table->string('url', 100)->index();
            $table->text('document');
            $table->string('caption_np', 255)->nullable();
            $table->string('caption_en', 255)->nullable();
            $table->unsignedInteger('order')->index();
            $table->boolean('status')->default(0);
            $table->timestamps();

            // Foreign Key
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('document');
    }
}
