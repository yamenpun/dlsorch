<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('fullname_np', 250);
            $table->string('fullname_en', 250);
            $table->string('designation_np', 250);
            $table->string('designation_en', 250);
            $table->string('division_np', 250);
            $table->string('division_en', 250);
            $table->string('email')->unique();
            $table->string('phone_no', 15);
            $table->text('image');
            $table->unsignedInteger('order')->index();
            $table->boolean('status')->default(1);
            $table->timestamps();

             // Foreign Key
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('staff');
    }
}
