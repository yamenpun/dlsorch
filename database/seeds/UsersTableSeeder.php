<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fullname_np'      => 'Super Admin Np',
            'fullname_en'      => 'Super Admin',
            'username'      => 'superadmin',
            'email'         => 'superadmin@gmail.com',
            'password'      => bcrypt('superadmin'),
            'role'          => 'administrator'
        ]);
    }

}
