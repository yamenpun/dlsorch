/*
SQLyog Community v12.2.4 (64 bit)
MySQL - 10.1.13-MariaDB : Database - isewa-project
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`isewa-project` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `isewa-project`;

/*Table structure for table `document` */

DROP TABLE IF EXISTS `document`;

CREATE TABLE `document` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_np` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `document` text COLLATE utf8_unicode_ci NOT NULL,
  `document_caption_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `document_caption_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `document_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_user_id_foreign` (`user_id`),
  KEY `document_url_index` (`url`),
  KEY `document_order_index` (`order`),
  CONSTRAINT `document_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `document` */

/*Table structure for table `gallery` */

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_np` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `gallery_url_index` (`url`),
  KEY `gallery_order_index` (`order`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `gallery` */

insert  into `gallery`(`id`,`created_at`,`updated_at`,`title_np`,`title_en`,`description_np`,`description_en`,`url`,`order`,`status`) values 
(5,'2016-06-13 08:38:51','2016-06-13 08:38:51','fdfdfd','fdfdf','<p>fdfdfdf</p>','<p>fdfdf</p>','fdfdf',1,1);

/*Table structure for table `gallery_images` */

DROP TABLE IF EXISTS `gallery_images`;

CREATE TABLE `gallery_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `gallery_id` int(10) unsigned NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `caption_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gallery_images_gallery_id_foreign` (`gallery_id`),
  KEY `gallery_images_order_index` (`order`),
  CONSTRAINT `gallery_images_gallery_id_foreign` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `gallery_images` */

insert  into `gallery_images`(`id`,`created_at`,`updated_at`,`gallery_id`,`image`,`caption_np`,`caption_en`,`order`,`url`) values 
(10,'2016-06-13 08:38:52','2016-06-13 08:57:58',5,'3872_Koala.jpg','fdfd','fdfdf',1,'fdfdf');

/*Table structure for table `link` */

DROP TABLE IF EXISTS `link`;

CREATE TABLE `link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `link_user_id_foreign` (`user_id`),
  KEY `link_url_index` (`url`),
  KEY `link_order_index` (`order`),
  CONSTRAINT `link_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `link` */

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL,
  `position` enum('header_menu','left_sidebar_menu','right_sidebar_men','footer_menu') COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `menu_url_index` (`url`),
  KEY `menu_order_index` (`order`),
  KEY `menu_position_index` (`position`),
  KEY `menu_status_index` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `menu` */

/*Table structure for table `menu_page` */

DROP TABLE IF EXISTS `menu_page`;

CREATE TABLE `menu_page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `menu_id` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `order` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_page_menu_id_foreign` (`menu_id`),
  KEY `menu_page_page_id_foreign` (`page_id`),
  KEY `menu_page_order_index` (`order`),
  CONSTRAINT `menu_page_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`),
  CONSTRAINT `menu_page_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `menu_page` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values 
('2014_10_12_000000_create_users_table',1),
('2014_10_12_100000_create_password_resets_table',1),
('2016_05_23_090245_create_document_table',1),
('2016_05_23_090440_create_link_table',1),
('2016_05_23_090613_create_notice_table',1),
('2016_05_23_090732_create_staff_table',1),
('2016_05_23_091212_create_menu_table',1),
('2016_05_23_091559_create_page_table',1),
('2016_05_23_091724_create_gallery_table',1),
('2016_05_23_091922_create_slider_table',1),
('2016_05_24_091416_create_gallery_images_table',1),
('2016_06_01_060247_create_menu_page_table',1),
('2016_06_05_092008_create_page_media_table',1),
('2016_06_05_100709_create_news_table',2);

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_np` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `caption_np` text COLLATE utf8_unicode_ci NOT NULL,
  `caption_en` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `news_user_id_foreign` (`user_id`),
  KEY `news_url_index` (`url`),
  CONSTRAINT `news_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `news` */

/*Table structure for table `notice` */

DROP TABLE IF EXISTS `notice`;

CREATE TABLE `notice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_np` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `image_caption_np` text COLLATE utf8_unicode_ci NOT NULL,
  `image_caption_en` text COLLATE utf8_unicode_ci NOT NULL,
  `image_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `notice_user_id_foreign` (`user_id`),
  KEY `notice_image_url_index` (`image_url`),
  KEY `notice_order_index` (`order`),
  CONSTRAINT `notice_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `notice` */

/*Table structure for table `page` */

DROP TABLE IF EXISTS `page`;

CREATE TABLE `page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_np` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description_en` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_np` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_en` longtext COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `page_page_url_index` (`url`),
  KEY `page_order_index` (`order`),
  KEY `page_status_index` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `page` */

insert  into `page`(`id`,`created_at`,`updated_at`,`title_np`,`title_en`,`slug`,`description_np`,`description_en`,`content_np`,`content_en`,`url`,`keywords`,`order`,`status`) values 
(1,'2016-06-15 05:39:51','2016-06-15 05:44:34','गृहपृष्ठ','Home','home','<p>नेपालको कृषि प्रणालीमा पशुपंक्षीपालन व्यवसायको महत्व अनादिकालदेखि आजसम्म रहि आएको छ । पशुपालन व्यबसायलाई विविधिकरण, व्यबसायीकरण, आयमुलक तथा सम्मानित पेशाको रुपमा विकास गरी राष्ट्रिय उत्पादनमा योगदान पर्&zwj;याउने पशु सेवा विभागको प्रमुख उदेश्य हो । बि.सं. १९९६ मा पहिलो पटक काठमाण्डौमा एलोप्याथिक प्रणाली अनुसार एक भेटेरिनरी डिस्पेन्सरीको स्थापना भयो र उक्त डिस्पेन्सरी बि.सं. १९९६ मा पशु चिकित्सालयमा परिणत भएको थियो । वि.सं. २०१४ साल सम्ममा १० जिल्लाहरुमा पशु अस्पतालहरुको स्थापना भयो ।</p>','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>\r\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>','<p>नेपालको कृषि प्रणालीमा पशुपंक्षीपालन व्यवसायको महत्व अनादिकालदेखि आजसम्म रहि आएको छ । पशुपालन व्यबसायलाई विविधिकरण, व्यबसायीकरण, आयमुलक तथा सम्मानित पेशाको रुपमा विकास गरी राष्ट्रिय उत्पादनमा योगदान पर्&zwj;याउने पशु सेवा विभागको प्रमुख उदेश्य हो । बि.सं. १९९६ मा पहिलो पटक काठमाण्डौमा एलोप्याथिक प्रणाली अनुसार एक भेटेरिनरी डिस्पेन्सरीको स्थापना भयो र उक्त डिस्पेन्सरी बि.सं. १९९६ मा पशु चिकित्सालयमा परिणत भएको थियो । वि.सं. २०१४ साल सम्ममा १० जिल्लाहरुमा पशु अस्पतालहरुको स्थापना भयो ।</p>','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>\r\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>','home','Home page',1,1),
(2,'2016-06-15 05:44:01','2016-06-15 05:45:26','सुचना','Notice','notice','<p>नेपालको कृषि प्रणालीमा पशुपंक्षीपालन व्यवसायको महत्व अनादिकालदेखि आजसम्म रहि आएको छ । पशुपालन व्यबसायलाई विविधिकरण, व्यबसायीकरण, आयमुलक तथा सम्मानित पेशाको रुपमा विकास गरी राष्ट्रिय उत्पादनमा योगदान पर्&zwj;याउने पशु सेवा विभागको प्रमुख उदेश्य हो । बि.सं. १९९६ मा पहिलो पटक काठमाण्डौमा एलोप्याथिक प्रणाली अनुसार एक भेटेरिनरी डिस्पेन्सरीको स्थापना भयो र उक्त डिस्पेन्सरी बि.सं. १९९६ मा पशु चिकित्सालयमा परिणत भएको थियो । वि.सं. २०१४ साल सम्ममा १० जिल्लाहरुमा पशु अस्पतालहरुको स्थापना भयो ।</p>','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>','<p>नेपालको कृषि प्रणालीमा पशुपंक्षीपालन व्यवसायको महत्व अनादिकालदेखि आजसम्म रहि आएको छ । पशुपालन व्यबसायलाई विविधिकरण, व्यबसायीकरण, आयमुलक तथा सम्मानित पेशाको रुपमा विकास गरी राष्ट्रिय उत्पादनमा योगदान पर्&zwj;याउने पशु सेवा विभागको प्रमुख उदेश्य हो । बि.सं. १९९६ मा पहिलो पटक काठमाण्डौमा एलोप्याथिक प्रणाली अनुसार एक भेटेरिनरी डिस्पेन्सरीको स्थापना भयो र उक्त डिस्पेन्सरी बि.सं. १९९६ मा पशु चिकित्सालयमा परिणत भएको थियो । वि.सं. २०१४ साल सम्ममा १० जिल्लाहरुमा पशु अस्पतालहरुको स्थापना भयो ।</p>','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>','notice','News Page',2,1),
(3,'2016-06-15 05:46:56','2016-06-15 05:46:56','समाचार','News','news','<p>नेपालको कृषि प्रणालीमा पशुपंक्षीपालन व्यवसायको महत्व अनादिकालदेखि आजसम्म रहि आएको छ । पशुपालन व्यबसायलाई विविधिकरण, व्यबसायीकरण, आयमुलक तथा सम्मानित पेशाको रुपमा विकास गरी राष्ट्रिय उत्पादनमा योगदान पर्&zwj;याउने पशु सेवा विभागको प्रमुख उदेश्य हो । बि.सं. १९९६ मा पहिलो पटक काठमाण्डौमा एलोप्याथिक प्रणाली अनुसार एक भेटेरिनरी डिस्पेन्सरीको स्थापना भयो र उक्त डिस्पेन्सरी बि.सं. १९९६ मा पशु चिकित्सालयमा परिणत भएको थियो । वि.सं. २०१४ साल सम्ममा १० जिल्लाहरुमा पशु अस्पतालहरुको स्थापना भयो ।</p>','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>','<p>नेपालको कृषि प्रणालीमा पशुपंक्षीपालन व्यवसायको महत्व अनादिकालदेखि आजसम्म रहि आएको छ । पशुपालन व्यबसायलाई विविधिकरण, व्यबसायीकरण, आयमुलक तथा सम्मानित पेशाको रुपमा विकास गरी राष्ट्रिय उत्पादनमा योगदान पर्&zwj;याउने पशु सेवा विभागको प्रमुख उदेश्य हो । बि.सं. १९९६ मा पहिलो पटक काठमाण्डौमा एलोप्याथिक प्रणाली अनुसार एक भेटेरिनरी डिस्पेन्सरीको स्थापना भयो र उक्त डिस्पेन्सरी बि.सं. १९९६ मा पशु चिकित्सालयमा परिणत भएको थियो । वि.सं. २०१४ साल सम्ममा १० जिल्लाहरुमा पशु अस्पतालहरुको स्थापना भयो ।</p>','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>','news','News page',3,1);

/*Table structure for table `page_media` */

DROP TABLE IF EXISTS `page_media`;

CREATE TABLE `page_media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `file` text COLLATE utf8_unicode_ci NOT NULL,
  `caption_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `page_media_page_id_foreign` (`page_id`),
  KEY `page_media_order_index` (`order`),
  CONSTRAINT `page_media_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `page_media` */

insert  into `page_media`(`id`,`created_at`,`updated_at`,`page_id`,`file`,`caption_np`,`caption_en`,`order`,`url`) values 
(1,'2016-06-15 05:39:51','2016-06-15 05:44:34',1,'9846_Desert.jpg','नेपालको कृषि प्रणालीमा','This is just for test',1,''),
(2,'2016-06-15 05:44:01','2016-06-15 05:45:26',2,'7000_Jellyfish.jpg','नेपालको कृषि प्रणालीमा','THis i',2,''),
(3,'2016-06-15 05:46:56','2016-06-15 05:46:56',3,'5970_Lighthouse.jpg','परिचय  नेपालको कृषि प्रणालीमा पशुपंक्षीपालन व्यवसायको महत्व अनादिकालदेखि आजसम्म रहि','This is just for test',3,'');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `slider` */

DROP TABLE IF EXISTS `slider`;

CREATE TABLE `slider` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `caption_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_np` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `slider_url_index` (`url`),
  KEY `slider_order_index` (`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `slider` */

/*Table structure for table `staff` */

DROP TABLE IF EXISTS `staff`;

CREATE TABLE `staff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `fullname_np` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `fullname_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `designation_np` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `designation_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `division_np` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `division_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_no` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `staff_email_unique` (`email`),
  KEY `staff_user_id_foreign` (`user_id`),
  KEY `staff_order_index` (`order`),
  CONSTRAINT `staff_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `staff` */

insert  into `staff`(`id`,`created_at`,`updated_at`,`user_id`,`fullname_np`,`fullname_en`,`designation_np`,`designation_en`,`division_np`,`division_en`,`email`,`phone_no`,`image`,`order`,`status`) values 
(1,'2016-06-15 04:51:13','2016-06-15 04:51:13',1,'श्री ज्ञान बहादुर थापा','Mr. Gyan Bahadur Thapa','प्रबन्धक','Manager','विज्ञापन','Marketing','gyan@gmail.com','9803231313','3473_pp1.jpg',1,1),
(3,'2016-06-15 05:00:46','2016-06-15 05:00:46',1,'श्री दिनेश बुढा ','Mr. Dinesh Budha','सह-प्रबन्धक','Assistant Manage','विज्ञापन ','Mar','dinesh@gmail.com','9803238882','2897_pp2.jpg',2,1),
(4,'2016-06-15 05:22:13','2016-06-15 05:22:13',1,'सुश्री पार्वती योन्जन','Ms. Parbati Yonjan','लेखापालन','Accountant','विज्ञापन ','Marketing','parbati@gmail.com','9803238231','7740_pp3.jpg',3,1);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` enum('administrator','normal') COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_index` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`fullname`,`username`,`email`,`password`,`role`,`remember_token`,`status`,`created_at`,`updated_at`) values 
(1,'Admin Admin','admin','admin@gmail.com','$2y$10$EVNqS4Ej3POHLRwtehu.BelpwKOigVJTh134zsXWTTgAbYqGoFeS.','administrator','6iLsrztArixIlNHFf4nDc6WmUuX2GVGotX6A8LQLr99b8nuGEUByJmyl1o2I',1,NULL,'2016-06-13 07:56:16');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
