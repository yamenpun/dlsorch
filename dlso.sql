/*
SQLyog Community v12.2.4 (64 bit)
MySQL - 10.1.13-MariaDB : Database - dlso
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dlso` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `dlso`;

/*Table structure for table `contact` */

DROP TABLE IF EXISTS `contact`;

CREATE TABLE `contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `address_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_box_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person_name_np` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_person_name_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_person_post_np` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_person_post_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `contact` */

insert  into `contact`(`id`,`address_np`,`address_en`,`phone_no`,`fax_no`,`post_box_no`,`email`,`contact_person_name_np`,`contact_person_name_en`,`contact_person_post_np`,`contact_person_post_en`,`created_at`,`updated_at`) values 
(3,'मन्थली, रामेछाप, नेपाल','Manthali, Ramechhap, Nepal','+977-04-8-54-0032','+977-04-8-54-0032','540032','info@dlsorch.gov.np','श्री धनसुन्दर प्र. साह','Mr. Dhansundar Pr. Shah','सूचना अधिकारी',' Information Officer','2016-07-17 05:39:32','2016-07-17 05:39:32');

/*Table structure for table `document` */

DROP TABLE IF EXISTS `document`;

CREATE TABLE `document` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_np` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `document` text COLLATE utf8_unicode_ci NOT NULL,
  `caption_np` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption_en` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `document_document_caption_en_unique` (`caption_en`),
  KEY `document_user_id_foreign` (`user_id`),
  KEY `document_url_index` (`url`),
  KEY `document_order_index` (`order`),
  CONSTRAINT `document_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `document` */

/*Table structure for table `gallery` */

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_np` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `feature_image` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `gallery_url_index` (`url`),
  KEY `gallery_order_index` (`order`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `gallery` */

insert  into `gallery`(`id`,`title_np`,`title_en`,`description_np`,`description_en`,`feature_image`,`url`,`order`,`status`,`created_at`,`updated_at`) values 
(1,'भवन उद्घाटन','Building Inaguration','','','feature_3276_dlso6.JPG','building-inaguration',1,1,'2016-07-15 16:22:42','2016-07-15 10:27:20'),
(2,'पशुपालन खेती','Livestock Farming','','','feature_5710_dlso2.jpg','livestock-farming',2,1,'2016-07-15 16:22:48','2016-07-15 10:27:58');

/*Table structure for table `gallery_images` */

DROP TABLE IF EXISTS `gallery_images`;

CREATE TABLE `gallery_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gallery_id` int(10) unsigned NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `caption_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `gallery_images_url_index` (`url`),
  KEY `gallery_images_order_index` (`order`),
  KEY `gallery_images_gallery_id_foreign` (`gallery_id`),
  CONSTRAINT `gallery_images_gallery_id_foreign` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `gallery_images` */

insert  into `gallery_images`(`id`,`gallery_id`,`image`,`caption_np`,`caption_en`,`url`,`order`,`status`,`created_at`,`updated_at`) values 
(11,1,'6252_dlso7.JPG','उद्घाटन पछिको तस्वीर','Picture after inaguration ','picture-after-inaguration',2,1,'2016-07-15 16:24:05','2016-07-15 10:39:05'),
(13,2,'5140_dlso2.jpg','गाईपालन - १ ','Cow Farming - 1','cow-farming-1',3,1,'2016-07-15 16:19:02','2016-07-15 10:34:02'),
(15,2,'7133_dlso9.JPG',' कुखुरा पालन - १ ','Chicken Farming - 1','chicken-farming-1',5,1,'2016-07-15 16:20:15','2016-07-15 10:35:15');

/*Table structure for table `introduction` */

DROP TABLE IF EXISTS `introduction`;

CREATE TABLE `introduction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_np` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `introduction` */

/*Table structure for table `link` */

DROP TABLE IF EXISTS `link`;

CREATE TABLE `link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `link_user_id_foreign` (`user_id`),
  KEY `link_url_index` (`url`),
  KEY `link_order_index` (`order`),
  CONSTRAINT `link_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `link` */

insert  into `link`(`id`,`user_id`,`title_np`,`title_en`,`url`,`order`,`status`,`created_at`,`updated_at`) values 
(1,2,'पशु सेवा तालीम तथा प्रसार निर्देशनालय','Directorate of Livestock Services Training & Extension','http://www.dlstraining.gov.np',1,1,'2016-07-10 10:27:06','2016-07-10 10:27:06'),
(2,2,'पशुपंक्षि बजार प्रवर्द्धन निर्देशनालय','Directorate of Livestock Market Promotion','http://www.dlmp.gov.np',2,1,'2016-07-10 10:28:53','2016-07-10 10:28:53');

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` tinyint(4) NOT NULL,
  `position` enum('header_menu','left_menu','footer_menu') COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `menu_order_index` (`order`),
  KEY `menu_position_index` (`position`),
  KEY `menu_status_index` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `menu` */

insert  into `menu`(`id`,`title`,`order`,`position`,`status`,`created_at`,`updated_at`) values 
(9,'Header Menu',1,'header_menu',1,'2016-07-08 11:26:30','2016-07-10 09:03:35'),
(24,'Left Menu',1,'left_menu',1,'2016-07-10 09:03:24','2016-07-10 09:03:24'),
(25,'Footer Menu',1,'footer_menu',1,'2016-07-10 09:04:00','2016-07-10 09:04:00');

/*Table structure for table `menu_page` */

DROP TABLE IF EXISTS `menu_page`;

CREATE TABLE `menu_page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `order` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `menu_page_menu_id_foreign` (`menu_id`),
  KEY `menu_page_page_id_foreign` (`page_id`),
  KEY `menu_page_order_index` (`order`),
  CONSTRAINT `menu_page_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE,
  CONSTRAINT `menu_page_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `menu_page` */

insert  into `menu_page`(`id`,`menu_id`,`page_id`,`parent_id`,`order`,`created_at`,`updated_at`) values 
(27,9,4,0,1,'2016-07-15 15:01:46','2016-07-15 15:01:46'),
(37,9,6,4,3,'2016-07-16 08:29:22','0000-00-00 00:00:00'),
(38,9,2,6,4,'2016-07-16 08:29:22','0000-00-00 00:00:00');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values 
('2014_10_12_000000_create_users_table',1),
('2014_10_12_100000_create_password_resets_table',1),
('2016_05_23_090245_create_document_table',1),
('2016_05_23_090440_create_link_table',1),
('2016_05_23_090613_create_notice_table',1),
('2016_05_23_090732_create_staff_table',1),
('2016_05_23_091212_create_menu_table',1),
('2016_05_23_091559_create_page_table',1),
('2016_05_23_091724_create_gallery_table',1),
('2016_05_23_091922_create_slider_table',1),
('2016_05_24_091416_create_gallery_images_table',1),
('2016_06_23_134012_create_menu_page_table',1),
('2016_06_29_060058_create_navigation_table',2),
('2016_07_11_115209_create_news_table',3),
('2016_07_15_114929_create_introduction_table',4),
('2016_07_15_115001_create_contact_table',4);

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_np` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `file` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `news_user_id_foreign` (`user_id`),
  KEY `news_url_index` (`url`),
  KEY `news_order_index` (`order`),
  CONSTRAINT `news_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `news` */

insert  into `news`(`id`,`user_id`,`title_np`,`title_en`,`description_np`,`description_en`,`file`,`url`,`order`,`status`,`created_at`,`updated_at`) values 
(9,2,'नेपालमा पशुपालनको महत्व ','Importance of Livestock Occupation','<p>नेपालमा पशुपालनको महत्व</p>','<p>Importance of Livestock Occupation</p>','1468495869_no_image.jpg','importance-of-livestock-occupation',1,1,'2016-07-12 10:35:14','2016-07-14 11:31:09');

/*Table structure for table `notice` */

DROP TABLE IF EXISTS `notice`;

CREATE TABLE `notice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_np` text COLLATE utf8_unicode_ci,
  `description_en` text COLLATE utf8_unicode_ci,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `caption_np` text COLLATE utf8_unicode_ci,
  `caption_en` text COLLATE utf8_unicode_ci,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `notice_user_id_foreign` (`user_id`),
  KEY `notice_order_index` (`order`),
  CONSTRAINT `notice_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `notice` */

/*Table structure for table `page` */

DROP TABLE IF EXISTS `page`;

CREATE TABLE `page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_np` longtext COLLATE utf8_unicode_ci,
  `content_en` longtext COLLATE utf8_unicode_ci,
  `page_type` enum('content-page','link-page') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'content-page',
  `image` text COLLATE utf8_unicode_ci,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `page_url_index` (`url`),
  KEY `page_order_index` (`order`),
  KEY `page_status_index` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `page` */

insert  into `page`(`id`,`title_np`,`title_en`,`content_np`,`content_en`,`page_type`,`image`,`url`,`order`,`status`,`created_at`,`updated_at`) values 
(1,'हाम्रो सेवाहरु ','Our Services','','','content-page','','our-services',1,1,'2016-06-30 05:47:36','2016-06-30 05:47:36'),
(2,'कम्प्युटर मर्मत ','Computer Repairing','','','content-page','','computer-repairing',2,1,'2016-06-30 05:48:22','2016-06-30 05:48:22'),
(3,'कम्प्युटर तालिम ','Computer Training','','','content-page','','computer-training',3,1,'2016-06-30 05:49:48','2016-06-30 05:49:48'),
(4,'हाम्रो बारेमा ','About Us','','','content-page','','about-us',4,1,'2016-06-30 05:51:25','2016-06-30 05:51:25'),
(6,'हाम्रो उद्देस्य ','Our Objectives','','','content-page','','our-objectives',5,1,'2016-07-17 10:51:20','2016-07-17 05:06:20'),
(7,'वेव पेज बनाउने ','Web Page Dvelopment','','','content-page','','web-page-dvelopment',6,1,'2016-07-17 10:51:29','2016-07-17 05:06:29');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

insert  into `password_resets`(`email`,`token`,`created_at`) values 
('superadmin@gmail.com','5e3583a015a8284b878055e883d14f4600c7f0a6d3bd0da5415b492f614bf9ab','2016-07-11 11:17:23'),
('nemayatinib@gmail.com','3d3e7420fb6fe6af0c0aba0fb6bb3e322ecf00861aedf8d388ed9ea04512c636','2016-07-15 11:46:25');

/*Table structure for table `slider` */

DROP TABLE IF EXISTS `slider`;

CREATE TABLE `slider` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `caption_np` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `slider_order_index` (`order`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `slider` */

/*Table structure for table `staff` */

DROP TABLE IF EXISTS `staff`;

CREATE TABLE `staff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `fullname_np` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `fullname_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `designation_np` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `designation_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `division_np` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `division_en` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_no` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `staff_email_unique` (`email`),
  KEY `staff_user_id_foreign` (`user_id`),
  KEY `staff_order_index` (`order`),
  CONSTRAINT `staff_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `staff` */

insert  into `staff`(`id`,`user_id`,`fullname_np`,`fullname_en`,`designation_np`,`designation_en`,`division_np`,`division_en`,`email`,`phone_no`,`image`,`order`,`status`,`created_at`,`updated_at`) values 
(2,2,'श्री ज्ञान बहादुर थापा','Mr. Gyan Bahadur Thapa','बरिष्ठ पशु विकास अधिकृत','Senior Veterinary Officer','पशु विभाग','Livestock','gyan@gmail.com','9802121211','2712_GyanBahadur.jpg',1,1,'2016-07-10 10:14:34','2016-07-10 10:14:34'),
(3,2,'श्री धनसुन्दर प्र. साह','Mr. Dhansundar Pr. Shah','सूचना अधिकारी ','Information Officer','पशु विभाग','Livestock','dhan@gmail.com','9821212121','3803_dhan.jpg',2,1,'2016-07-10 10:19:40','2016-07-10 10:21:06'),
(4,2,'सुश्री पार्वती योन्जन','Ms. Parbati Yonjan','सहायक सूचना अधिकारी ','Assistant Information Officer','पशु विभाग ','Livestock','parbati@gmail.com','9802121211','2075_Parbati.jpg',3,1,'2016-07-10 10:23:49','2016-07-10 10:23:49');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname_np` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `fullname_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` enum('administrator','normal') COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_index` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`fullname_np`,`fullname_en`,`username`,`email`,`password`,`role`,`remember_token`,`status`,`created_at`,`updated_at`) values 
(2,'ज्ञान बहादुर थापा ','Gyan Bahadur Thapa','superadmin','superadmin@gmail.com','$2y$10$5dxp/UsZCLPxZgLPRUyQKOFf9641W2B6P.apzdzIG9C9xz5FC96CC','administrator','vQ3bZbZy15aip6w80MZY1V4S7sjf2mjyktcgxZOMUv1x6k6OqN75MMMEjJP2',1,'2016-07-16 20:27:01','2016-07-16 14:42:01'),
(10,'admin','admin','newadmin','nemayatinib@gmail.com','$2y$10$ph9bW1MTzOCrJosyM/fMj.CUVvkYXUx.vlR8JApZ3T44w5lYjp3N6','normal','XIVkSW3l8mQmoywPtpTRin5VJFReeGDPPXKWAcUMoYLoz80aYIWo4jJ498dH',1,'2016-07-11 05:56:56','2016-07-11 11:18:19');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
